import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Storage } from '@capacitor/storage';
import { Injectable } from '@angular/core';

import { NGXLogger } from 'ngx-logger';

import { Keys } from './keys';
import { environment } from 'src/environments/environment';

@Injectable()
export class Global {
  // FIXME: Orders get saved having key "matches" and missing key "date"

  public readonly __settingsVersion = 1;
  public readonly __appVersion: Version = {major: 0, minor: 0, patch: 3};
  public          __appVersionStored: Version;
  public readonly __isProduction = environment.production;

  public readonly __serverDomainName: string = environment.serverDomain;
  
  public ordersList: Array<Object> = [];

  private readonly __defaultAppSettings: AppSettings = { settingsVersion: this.__settingsVersion, useBiometricLogin: false, userInterface: { theme: "light" }, mapSettings: { caching: 0, rendering: { dimensions: 2, terrainType: "default" }, showTraffic: false, optimizeRoute: false } };
  public appSettings: AppSettings = this.__defaultAppSettings;

  /**
   * Stored business that have been used in the last 30 days.
   * Used to try get business data locally which is faster and cheaper, 
   * before asking server side if it has any business data that may match.
   */
  private businessData: Array<object> = [];
  /*
    [
      {
        date: new Date.now(),
        business: { 
          name: "Insomnia - Baseline",
          address: "5389 E Main St",
          city: "Hillsboro",
          state: "OR",
          zip: 97123,
          mapsData: { 
            date: new Date.now(),
            placeID: string
            coords: {
              lat: number,
              lng: number
            }
          }
        }
      },
      ...
    ]
  */

  /**
   * Used to collect data to help refine app behavior and performance
   */
  public appOpStats = {
    geolocation: {
      accuracy: {
        navigation: {
          // An array of numbers representing the GPS its accuracy predictions in meters
          data: [],
          
          processData: () => {
            const thisObject = this.appOpStats.geolocation.accuracy.navigation;
            const processedData = {
              median: undefined,
              mean: 0, 
              mode: {},
              range: {min: 9000, max: 0},
            };

            for (const range of thisObject.data) {
              // Add up all numbers to calculate the mean later
              processedData.mean = processedData.mean + range;

              const rangeTenStep = Math.floor(range / 10);
              processedData.mode[rangeTenStep] = (processedData.mode[rangeTenStep] | 0) + 1;

              // Update max range
              if (range > processedData.range.max) processedData.range.max = range;

              // Update min range
              if (range < processedData.range.min) processedData.range.min = range;
            }

            // Calculate the mean
            processedData.mean = Math.round(processedData.mean / thisObject.data.length);

            this.appOpStats.geolocation.accuracy.navigation.data = [];

            return JSON.stringify(processedData);
          }
        }
      }
    }
  };

  constructor (
    private httpClient: HttpClient,
    private logger: NGXLogger
  ) { }

  public API_KEYS = Keys.API_KEYS;

  public async checkAppVersion(): Promise<any> {
    const storedKeys = await Storage.keys();

    if (!storedKeys.keys.includes("appVersion")) {
      this.__appVersionStored = { major: 0, minor: 0, patch: 0 };

      this.logger.info("Global: appVersion was set");

      return;
    } else {
      // Else get it
      await Storage.get({ key: "appVersion" })
      .then(storedAppVersion => {
        this.logger.info("Global: appVersion was loaded");
        this.__appVersionStored = JSON.parse(storedAppVersion["value"]);
      })
      .catch(storageError => {
        this.logger.error("Global: appVersion could not be loaded from storage");
        this.logger.error(storageError);

        // Set stored version to current version to prevent errors from cascading
        this.__appVersionStored = this.__appVersion;
      });

      return;
    }
  }

  /**
   * Adds or updates the stored appVersion to the hard-coded version
   */
  public async setAppVersion() {
    const storedKeys = await Storage.keys();
    
    // Add appVersion if it doesn't exist in storage or is out of date
    await Storage.set({
      key: "appVersion", 
      value: JSON.stringify(this.__appVersion) 
    }).catch((storageError) => {
      this.logger.error("Global: appVersion could not be saved");
      this.logger.error(storageError);
    });
  }

  public async checkForUpdates(): Promise<any> {
    // TODO: Check for application update via server or Google Play
  }

  /**
   * This function only gets called upon app initialization. It fetches all stored values, 
   * does a few error-safety checks, and then applies the loaded setting and global
   * variables.
   */
  public async initApp() {
    // TODO: Check for permissions: read, write, geolocation
    //       Ask user for permissions
    
    const storedKeys = await Storage.keys();

    // Checks if appSettings exists in storage
    if (!storedKeys.keys.includes("appSettings")) {
      this.logger.info("Global: No stored keys");

      this.appSettings = this.__defaultAppSettings;

      this.logger.info("Global: appSettings was set");

      this.saveSettings();
    } else {
      // Else create it and add it to storage
      await Storage.get({ key: "appSettings" })
      .then(storedAppSettings => {
        this.logger.info("Global: appSettings was loaded");
        this.appSettings = JSON.parse(storedAppSettings["value"]);
      })
      .catch(storageError => {
        this.logger.error("Global: appSettings could not be loaded from storage");
        this.logger.error(storageError);

        // Load in default as a fail-safe
        this.appSettings = this.__defaultAppSettings;
      });
    }

    // Checking for user's theme settings
    const prefersDark = window.matchMedia('(prefers-color-scheme: dark)');

    // Listen for changes to theme settings
    prefersDark.addEventListener("change", (mediaQuery) => {
      if (this.appSettings["userInterface"]["theme"] === "system") this.toggleDarkTheme(prefersDark.matches);
    });

    // Apply the users theme settings
    switch (this.appSettings["userInterface"]["theme"]) {
      case "system": this.toggleDarkTheme(prefersDark.matches); break;
      case "light": this.toggleDarkTheme(false); break;
      case "dark": this.toggleDarkTheme(true); break;
      default: this.logger.warn("Global: An unknown string was given for the theme setting"); break;
    }
    
    // Fetch stored ordersList if it exists
    if (storedKeys.keys.includes("ordersList")) {
      await Storage.get({ key: "ordersList" })
      .then(storedOrdersList => {
        const currentTime = Date.now();
        const storedOrdersListValue = JSON.parse(storedOrdersList.value);

        // If the time ordersList was saved is less than 12 hours ago, use it
        if (currentTime < (storedOrdersListValue["date"] + 43200000)) {
          this.logger.info("Global: ordersList was loaded");
          this.ordersList = storedOrdersListValue.data;
        } else {
          // Else delete it
          this.logger.info("Global: The saved ordersList has expired");
          this.saveOrdersList();
        }
      })
      .catch(storageError => {
        this.logger.error("Global: ordersList could not be loaded from storage");
        this.logger.error(storageError);
      });
    }

    // Fetch stored businessesData if it exists
    if (storedKeys.keys.includes("businessData")) {
      await Storage.get({ key: "businessData"})
      .then((storedBusinessData) => {
        const currentTime = Date.now();
        const storedBusinessDataValue = JSON.parse(storedBusinessData.value);

        for (let businessIndex in storedBusinessDataValue) {
          const businessEntry = storedBusinessDataValue[businessIndex];

          // Only add business that have been used in the last 30 days to the businessData array
          if (currentTime < (businessEntry["date"] + 2592000000)) {
            this.businessData.push(businessEntry);
          }
        }

        this.logger.info("Global: businessData was loaded");

        // Save the businessData array
        this.saveBusinessData();
      })
      .catch((storageError) => {
        this.logger.error("Global: businessData could not be loaded from storage");
        this.logger.error(storageError);
      });
    }
  }

  public async saveSettings() {
    this.logger.info("Global: appSettings is being saved");

    // TODO: Return or throw, so that settings.page can use that to show an error message
    await Storage.set({
      key: "appSettings",
      value: JSON.stringify(this.appSettings)
    }).catch(storageError => {
      this.logger.error("Global: appSettings could not be saved");
      this.logger.error(storageError);
    });
  }

  public async saveOrdersList() {
    this.logger.info("Global: ordersList is being saved");
    
    const dateSaved = Date.now();

    await Storage.set({
      key: "ordersList",
      value: JSON.stringify({
        date: dateSaved,
        data: this.ordersList
      })
    }).catch(storageError => {
      this.logger.error("Global: ordersList could not be saved");
      this.logger.error(storageError);
    });
  }

  private async saveBusinessData() {
    this.logger.info("Global: businessData is being saved");

    await Storage.set({
      key: "businessData",
      value: JSON.stringify(this.businessData)
    }).catch(storageError => {
      this.logger.error("Global: businessData could not be saved");
      this.logger.error(storageError);
    });
  }

  // FIXME: BusinessData will get saved with matches key instead of date
  public saveBusiness(providedBusiness: object) {
    const businessEntry = {
      date: Date.now(),
      business: providedBusiness
    };

    let isBusinessMatched = false;

    // Loop through businesses in businessData
    for (let businessIndex in this.businessData) {
      const savedBusiness = this.businessData[businessIndex]["business"];
      let isAddressMatched = true;
      let isNameMatched = true;

      // Try to match saved and provided business address
      for (let savedAddressKey in savedBusiness) {
        for (let providedAddressKey in providedBusiness) {
          // If both loops are on the same key
          if (savedAddressKey === providedAddressKey) {
            switch (savedAddressKey) {
              case "address":
              case "address2":
              case "city":
              case "state":
              case "zip":
                if (savedBusiness[savedAddressKey] !== providedBusiness[providedAddressKey])
                  isAddressMatched = false;

                break;
              case "name":
                if (savedBusiness["name"] !== providedBusiness["name"])
                  isNameMatched = false;

                break;
            }
          }
        }
      }

      //  Checks if the name or address of an existing stored business was changed
      if (isAddressMatched || isNameMatched) {
        this.businessData.splice(Number.parseInt(businessIndex), 1, businessEntry);
        this.saveBusinessData();

        isBusinessMatched = true;

        break;
      }
    }

    // If business is not recognized, save it
    if (!isBusinessMatched) {
      this.businessData.push(businessEntry);
      this.saveBusinessData();
    }
  }

  public lastServerBusinessQuery: number;
  
  public searchForBusiness(providedBusiness: object): Promise<object[]> {
    const lifeIsPain = new Promise<object[]>((resolve, reject) => {
      let matchingBusinesses: object[] = [];

      // Iterate through keys in the provided business that needs to be searched for
      for (let providedBusinessKey in providedBusiness) {
        // Iterate through the list of locally stored businesses
        for (let storedBusinessIndex in this.businessData) {
          const businessEntry = this.businessData[storedBusinessIndex]["business"];

          // Iterate through keys of the locally stored business
          for (let storedBusinessKey in businessEntry) {
            // Make sure both object-key loops are on the same key
            if (providedBusinessKey === storedBusinessKey) {
              const storedBusinessValue = JSON.stringify(businessEntry[storedBusinessKey]);
              let providedBusinessValue = JSON.stringify(providedBusiness[providedBusinessKey]);
              providedBusinessValue = providedBusinessValue.substring(1, providedBusinessValue.length - 1);

              // Don't process empty keys with empty values
              if (providedBusinessValue.length > 2 && storedBusinessValue.length > 2) {
                // Make matching case-insensitive
                const regex = new RegExp(providedBusinessValue, "i");

                // Check if the stored business could match
                if (storedBusinessValue.match(regex)) {
                  let isDuplicateBusiness = false;

                  // Iterate through the matchingBusinessesList to check if the current business has already been added
                  for (let matchedIndex in matchingBusinesses) {
                    const matchingBusinessPlaceID = matchingBusinesses[matchedIndex]["business"]["mapData"]["placeID"];
                    const currentBusinessPlaceID = this.businessData[storedBusinessIndex]["business"]["mapData"]["placeID"];

                    if (currentBusinessPlaceID === matchingBusinessPlaceID) {
                      isDuplicateBusiness = true;

                      // Tally up the amount of matched fields
                      matchingBusinesses[matchedIndex]["matches"]++;
                    }
                  }
                  
                  // Add the business that has a matching part to the list of matching businesses list, only if it hasn't been added
                  if (!isDuplicateBusiness) {
                    const matchedBusiness: object = this.businessData[storedBusinessIndex];
                    delete matchedBusiness["date"];
                    matchedBusiness["matches"] = 1;

                    matchingBusinesses.push(matchedBusiness);
                  }
                }
              }
            }
          }
        }
      }

      // FIXME: If Firestore read ops becomes a problem consider the possible provided solutions in this file
      //  - Restrict timing of pulling matching businesses from the server
      //  - Store them on the device
      //  - Require a certain amount fields to be filled out

      const serverBusinessQuery = () => {
        const postContent: object = {
          requestType: "business",
          providedBusiness
        };
    
        // TODO: Pull list of potential matching businesses from the cloud
        this.httpClient.post(this.__serverDomainName + "/fetch", postContent)
          .subscribe(
            result => {
              // TODO: Check results with an interface and then concat
              
              matchingBusinesses = matchingBusinesses.concat(result);
            },
            error => {
              this.logger.warn("searchForBusiness(): An error was encountered fetching businesses from the server.");
              this.logger.warn(error);

              reject(error);
            },
            () => {
              // Finally
              // TODO: Sort matchingBusiness[#]["matches"] by its value
              ///@ts-ignore
              matchingBusinesses.sort((a, b) => a.matches > b.matches ? -1 : a.matches < b.matches ? 1 : 0);

              // TODO: Remove duplicate entries
              // What's the point of storing business locally?
                // For referencing. When fetching a route, only the placeID would need to be given to check if the app has it locally stored.
                // It stores it locally so that when the app is restarted with no network service available that the app still functions
              // TODO: BusinessEntries need a checksum

              resolve(matchingBusinesses);
            }
          );
      };

      if (!this.lastServerBusinessQuery) {
        this.lastServerBusinessQuery = Date.now();
        
        serverBusinessQuery();
      } // Else-if its 5 seconds since the last server query
      else if (Date.now() > (this.lastServerBusinessQuery + 5000)) {
        this.lastServerBusinessQuery = Date.now();
        
        serverBusinessQuery();
      }
    });
    
    return lifeIsPain;
  }

  private toggleDarkTheme(isDark: boolean) {
    // FIXME: Dark Mode does not toggle on Android and iOS. 
    // Seems like like Capacitor doesn't make the value available the way I did it.
    // Check the docks again
    document.body.classList.toggle("dark", isDark);
  }
}

interface Version {
  major: number,
  minor: number,
  patch: number
};

export enum ListMode {
  MODIFY = "modify",
  REORDER = "reorder",
  NAVIGATE = "navigate",
  COMPLETED = "completion"
}

export enum OrderStatus {
  PENDING = 0,
  IN_PROGRESS,
  AT_DESTINATION,
  COMPLETED
}

// TODO: Complete interfaces
export interface OrderEntry {

}

export interface OrderData {
  
}

export interface OrderBusiness {
  
}

export interface OrderMetadata {
  
}

export interface AppSettings {
  settingsVersion: number, // this.__settingsVersion
  useBiometricLogin: boolean,
  userInterface: {
    theme: "system" | "light" | "dark"
  },
  mapSettings: {
    caching: number // Number that represents a certain amount of data storage,
    rendering: {
      dimensions: 2 | 3,
      terrainType: "default" | "satellite" // satellite = hybrid
    },
    showTraffic: boolean,
    optimizeRoute: boolean
  }
}