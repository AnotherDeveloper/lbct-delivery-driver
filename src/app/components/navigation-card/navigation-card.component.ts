import { Component, EventEmitter, Input, IterableDiffer, OnInit, Output, IterableDiffers, IterableChanges, KeyValueDiffer, KeyValueDiffers, KeyValueChanges, SimpleChange } from '@angular/core';

import { OrderStatus } from 'src/global';

import { NGXLogger } from 'ngx-logger';
import { TTSVoice } from '@awesome-cordova-plugins/text-to-speech-advanced/ngx';
import TTS from 'cordova-plugin-tts-advanced/www/tts';

@Component({
  selector: 'app-navigation-card',
  templateUrl: './navigation-card.component.html',
  styleUrls: ['./navigation-card.component.scss'],
})
export class NavigationCardComponent implements OnInit {
  
  @Input("orders") ordersList: object[] = [];
  private ordersListIterableDiff: IterableDiffer<object>;
  private ordersListKeyValDiff: KeyValueDiffer<any, any>[] = [];
  @Output('ordersChange') ordersListChange = new EventEmitter<object[]>();

  // Determines if the map should show location markers or a route
  @Input() listMode: string;

  // Navigation Data
  @Input() navigationData: object;
  private navigationDataKeyValDiff: KeyValueDiffer<any, any>[] = [];
  @Output('navDataChange') navigationDataChange = new EventEmitter<object>();

  constructor(
    private iterableDiffers: IterableDiffers,
    private keyValueDiffers: KeyValueDiffers,
    private logger: NGXLogger
  ) {}

  public businessName: string = "";
  public navigationInstructions: string;
  // Index to current order in ordersList
  public currentOrder: number = 0;
  // Tells the template what data/UI needs to be rendered
  public navigationStage: number = 0;
  // Index to navigation instructions in navigationData[routes][0][currentOrder].steps[THIS]
  public navigationRouteStep: number = 0;
  public confirmationRequired: boolean = false;

  // To Next Stop
  public nextDistance: number = 0;
  public nextTime: number = 0;
  public nextHour: number = 0;

  // Route Remainder
  public routeDistance: number = 0;
  public routeTime: number = 0;
  public routeHour: number = 0;

  // Device Voices
  private voices: TTSVoice[];

  public ngOnInit() {
    this.ordersListIterableDiff = this.iterableDiffers.find(this.ordersList).create();

    this.businessName = this.ordersList[this.currentOrder]['business']['name'];

    document.addEventListener('deviceready', () => {
      this.logger.error("Device ready event listener was fired");

      TTS.getVoices()
      .then((voices: TTSVoice[]) => {
        voices.forEach((voice) => {
          if (voice.language === "en_US") {
            voices.push(voice);
          }
        });
      })
      .catch((error) => {
        this.logger.warn(error);
      });

      // Voice Identifiers
        // en-us-x-iom-(network | local)  Male
        // en-us-x-iol-network            Male
        // en-us-x-tpd-network            Male

        // en-us-x-tpf-network            Female Highest
        // en-us-x-iog-network            Female
        // en-us-x-iob-network            Female
        // en-us-x-tpc-network            Female Deeper
        // en-us-x-sfg-network            Female Robotic
    });
  }

  private loadKeyValueDiffers() {
    if (this.ordersListKeyValDiff.length === 0) {
      this.ordersList.forEach((order) => {
        this.ordersListKeyValDiff.push(this.keyValueDiffers.find(order["metadata"]).create());
      });
    }



    if (this.navigationDataKeyValDiff.length === 0 && this.navigationData) {
      this.navigationDataKeyValDiff.push(this.keyValueDiffers.find(this.navigationData["routes"][0]).create());

      this.isConfirmationRequired();
      this.loadKeyValueDiffers();
      this.updateUIValues();
    }
  }

  public ngOnChanges(changes: SimpleChange) {
    // Load kVDiff for navigationData once it's ready
    if (this.navigationData && this.navigationData["code"] === "Ok") {
      this.loadKeyValueDiffers();
    }
  }
  
  public ngDoCheck() {
    const ordersListChange: IterableChanges<object> = this.ordersListIterableDiff.diff(this.ordersList);

    if (ordersListChange) {
      if (ordersListChange) this.ordersListKeyValDiff = [];

      this.loadKeyValueDiffers();
    }

    this.ordersListKeyValDiff.forEach((orderDiff, index) => {
      const orderChange: KeyValueChanges<any, any> = orderDiff.diff(this.ordersList[index]["metadata"]);

      if (orderChange) {
        orderChange.forEachChangedItem((changedKey) => {
          switch (changedKey.key) {
            case "delivery_status":
              this.navigationStage = changedKey.currentValue;

              // Gets navigation card ready for next destination.
              if (this.navigationStage === 3)  {
                this.currentOrder++;

                if (this.currentOrder < (this.navigationData["routes"][0]["legs"].length - 1)) {
                  this.businessName = this.ordersList[this.currentOrder]['business']['name'];
                  this.isConfirmationRequired();
                } else {
                  this.businessName = "Longbottom Warehouse";
                }
                
                this.navigationStage = 0;
                this.navigationRouteStep = 0;
              }

              break;
            default:
              this.logger.warn(`Nav Card component: ngDoCheck(): Unknown order object key "${changedKey.key}" was processed`);

              break;
          }
        });
      }
    });
    
    this.navigationDataKeyValDiff.forEach((navDataDiff, index) => {
      const navDataChange: KeyValueChanges<any, any> = navDataDiff.diff(this.navigationData["routes"][0]["legs"][this.currentOrder]);

      if (navDataChange) {
        navDataChange.forEachChangedItem((changedKey) => {
          switch (changedKey.key) {
            case "current_step":
              this.navigationRouteStep = changedKey.currentValue + 1;
              
              setTimeout(() => this.voiceInstructions(), 500);

              break;
            default:
              this.logger.warn(`Nav Card component: ngDoCheck(): Unknown navData object key "${changedKey.key}" was processed`);

              break;
          }

          this.updateUIValues();
        })
      }
    });
  }

  /**
   * Vocally tells the user navigation instructions
   */
  private voiceInstructions() {
    this.logger.trace("Navigation Card component: voiceInstructions() was invoked")
    // TODO: Let user disable voice instructions

    // Stop voice instructions if TTS couldn't initialize properly
    if (!this.voices || this.voices.length === 0) {
      this.logger.warn("voiceInstructions: There are no voices available");
      
      return;
    }

    TTS.speak({
      text: this.navigationInstructions,
      identifier: this.voices[0].identifier
    })
      .then((resolve) => {
        console.log(resolve);
        console.log("TTS Resolved");
      })
      .catch((error: Error) => {
        console.error(error.message);
        console.error(error);
      });
  }

  private updateUIValues() {
    // TODO: Update remaining time and distance

    this.nextDistance = Number.parseFloat((this.navigationData["routes"][0]["legs"][this.currentOrder]["distance"] / 1607.34).toFixed(2));
    this.nextTime = Math.round(this.navigationData["routes"][0]["legs"][this.currentOrder]["duration"] / 60);
    this.nextHour = Date.now() + (this.navigationData["routes"][0]["legs"][this.currentOrder]["duration"] * 1000);

    let routeDistance = 0;
    let routeTime = 0;

    for (let routeLeg of this.navigationData["routes"][0]["legs"]) {
      routeDistance += routeLeg["distance"];
      routeTime += routeLeg["duration"];
    }

    this.routeDistance = Number.parseFloat((routeDistance / 1607.34).toFixed(2));
    this.routeTime = Math.round(routeTime / 60);
    this.routeHour = Date.now() + (routeTime * 1000);
    
    this.navigationInstructions = this.navigationData["routes"][0]["legs"][this.currentOrder]["steps"][this.navigationRouteStep]["maneuver"]["instruction"];
  }

  /**
   * A function for determining if user attention and intervention is required
   */
  private isConfirmationRequired() {
    const orderData = this.ordersList[this.currentOrder]['orderData'];

    if (orderData["paymentType"] === 'cod' || orderData["posters"] || orderData["pickUps"]) 
      this.confirmationRequired = true;
    else
      this.confirmationRequired = false;
  }

  /**
   * This function marks the meta-data of the current order as completed. 
   * It is only used on orders with payment type of COD, posters/envelopes, and customer care forms.
   * Any orders that don't meet that criteria are assumed fully fulfilled and marked completed by the
   * Mapbox Maps component.
   */
  public completeOrder() {
    this.ordersList[this.currentOrder]["metadata"]["delivery_status"] = OrderStatus.COMPLETED;
    this.ordersListChange.emit(this.ordersList);
  }
}
