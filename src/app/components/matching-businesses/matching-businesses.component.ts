import { Component, Input, OnInit, Output, SimpleChanges, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-matching-businesses',
  templateUrl: './matching-businesses.component.html',
  styleUrls: ['./matching-businesses.component.scss'],
})
export class MatchingBusinessesComponent implements OnInit {

  @Input('matches') matchingBusinesses: object[];
  @Output() selectedBusiness = new EventEmitter<number>();

  constructor() { }

  ngOnInit() {}

  public businessClicked(businessListIndex: number) {
    this.selectedBusiness.emit(businessListIndex);
  }
}
