import { FormGroup, FormControl, ValidatorFn, AbstractControl, ValidationErrors } from '@angular/forms';
import { Component, EventEmitter, Input, OnInit, Output, SimpleChanges, ViewChildren, ChangeDetectorRef } from '@angular/core';
import { NGXLogger } from 'ngx-logger';
import { ItemReorderEventDetail } from '@ionic/core';
import { IonItemSliding, ModalController } from '@ionic/angular';

import { ManualEntryPage } from '../../pages/delivery/modals/manual-entry/manual-entry.page';

@Component({
  selector: 'app-orders-card',
  templateUrl: './orders-card.component.html',
  styleUrls: ['./orders-card.component.scss'],
})
export class OrdersCardComponent implements OnInit {

  // List of orders
  @Input('orders') ordersList: Array<object> = [];
  @Output('ordersChange') ordersListChange = new EventEmitter<Array<object>>();
  // The way that the user can interact with the ordersList
  @Input('listMode') listMode: string;

  // If modifyDisabled is false, the user can edit or delete orders from the list
  modifyDisabled = true;
  // If reorderDisabled is false, the user can change the order of the orders to adjust route generation
  reorderDisabled = true;

  orderValidator: ValidatorFn = (control: AbstractControl): ValidationErrors | null => {
    const boxes = control.get("boxes");
    const boxesLoaded = control.get("boxesLoaded");
    const valves = control.get("valves");
    const valvesReceived = control.get("valvesReceived");
    const invoice = control.get("invoice");

    if (typeof this.ordersListIndex !== "undefined") {
      // At least one field, besides invoice, has to be filled out
      if (boxes.value > 0 || valves.value > 0) {
        if (invoice.value && (!boxes.value || boxesLoaded.value) && (!valves.value || valvesReceived.value)) {
          // There are contents in the order and the invoice has been collected!
          this.ordersList[this.ordersListIndex]["metadata"]["status"] = "complete";

          this.ordersListChange.emit(this.ordersList);
          
          return null;
        } else {
          // Invoice is missing, status pending
          this.ordersList[this.ordersListIndex]["metadata"]["status"] = "pending";
          
          this.ordersListChange.emit(this.ordersList);

          return { "status": "pending" };
        }
      } else {
        // There are no contents in the order, status pending
        this.ordersList[this.ordersListIndex]["metadata"]["status"] = "pending";
        
        this.ordersListChange.emit(this.ordersList);

        return { "status": "pending" };
      }
    }
  };

  constructor(
    private modalController: ModalController,
    private logger: NGXLogger,
    private changeDetectRef: ChangeDetectorRef
  ) {
    this.logger.info("Route Card component was used");
  }

  private ordersListIndex: number;
  public orderData = new FormGroup({
    boxes: new FormControl(''),
    boxesLoaded: new FormControl(false),
    valves: new FormControl(''),
    valvesReceived: new FormControl(false),
    invoice: new FormControl(false)
  }, {validators: this.orderValidator});

  // TODO: Open the file that contains this TODO to view a list

  // Order status for server "served" orders
  // Need to be able to click an order and check off items in an order for served orders
  // Animate item-sliding to show that it's a sliding item
  public ngOnInit() {
    this.setElementListMode();

    // Initializes showOrderContents[]
    if (this.showOrderContentsHeight.length !== this.ordersList.length) {
      for (var showLength = this.ordersList.length; this.showOrderContentsHeight.length < this.ordersList.length; showLength++) {
        this.showOrderContentsHeight.push(false);
        this.showOrderContentsDisplay.push(false);
      }
    }
  }

  // Called upon this element its attributes being changed
  public ngOnChanges(changes: SimpleChanges) {
    // Update the listing mode
    this.setElementListMode();
  }

  // Gets all elements of ion-item-sliding type
  @ViewChildren(IonItemSliding) 
  private slidingItems: IonItemSliding[];

  private setElementListMode() {
    switch (this.listMode) {
      case "modify": // Enables ion-item-sliding and disables all other elements
        this.modifyDisabled = false;
        this.reorderDisabled = true;

        break;
      case "reorder": // Enables ion-reorder, disables sliding-item, and closes order content panes
        // Closes any open order contents pane
        for (let orderContents in this.showOrderContentsDisplay) {
          this.showOrderContentsDisplay[orderContents] = false;
        }

        // Loops through all ion-item-sliding elements
        this.slidingItems.forEach((itemSliding) => {
          // Slides them back closed
          itemSliding.close();
        });

        this.modifyDisabled = true;
        this.reorderDisabled = false;

        break;
      default:
        this.logger.warn("Route Card component: setElementListMode(): Unknown listMode was processed");

        break;
    }
  }

  // FIXME: ordersList and template get out of sync when re-ordering. Don't know what class or function is causing it
  public doReorder(event: Event | CustomEvent<ItemReorderEventDetail>) {
    this.logger.info('Route Card component: Dragged from index', event["detail"].from, 'to', event["detail"].to);

    this.ordersList = event["detail"].complete(this.ordersList);
    this.ordersListChange.emit(this.ordersList);
  }

  public showOrderContentsHeight: Array<boolean> = [];
  public showOrderContentsDisplay: Array<boolean> = [];

  /**
   * Toggles an order's contents pane open
   */
  public pressedOrderEntry(ordersListIndex: number) {
    // Don't fire in reorder mode
    if (this.listMode === 'reorder') return;
    if (!this.ordersList[ordersListIndex]["orderData"]["hasMerch"]) return;

    // Extends showOrderContents[] when an order is added
    if (this.showOrderContentsHeight.length !== this.ordersList.length) {
      for (var showLength = this.ordersList.length; this.showOrderContentsHeight.length < this.ordersList.length; showLength++) {
        this.showOrderContentsHeight.push(false);
        this.showOrderContentsDisplay.push(false);
      }
    }

    this.ordersListIndex = ordersListIndex;

    // Load stored order data to be shown in the selected order's content pane
    for (let formOrderKey in this.orderData.value) {
      switch (formOrderKey) {
        case "boxesLoaded":
        case "boxes":
        case "valvesReceived":
        case "valves":
          this.orderData.get(formOrderKey).setValue(this.ordersList[ordersListIndex]["orderData"]["hasMerch"][formOrderKey]);

          break;
        case "invoice":
          this.orderData.get(formOrderKey).setValue(this.ordersList[ordersListIndex]["orderData"][formOrderKey]);

          break;
        default:
          this.logger.warn(`Route Card component: pressedOrderEntry(): An unknown formControl (${formOrderKey}) was processed`);

          break;
      }
    }

    if (!this.showOrderContentsHeight[ordersListIndex]) {
      // Toggles the selected order style.display. Enabling the rendering of its inner contents
      this.showOrderContentsDisplay[ordersListIndex] = true;
      // Toggles the selected order style.height with a 0.1 second delay. Else the transition animation won't play
      // Expands the order contents view
      setTimeout(() => this.showOrderContentsHeight[ordersListIndex] = true, 100);
    } else {
      // Toggles the selected order style.display with a 1.5 second delay. Disabling the rendering of its inner contents
      setTimeout(() => this.showOrderContentsDisplay[ordersListIndex] = false, 1100);
      // Toggles the selected order style.height. Shrinks the order contents view
      this.showOrderContentsHeight[ordersListIndex] = false;
    }

    // Limit the user to see the contents of one order only
    this.showOrderContentsHeight.forEach((showOrderValue, index) => {
      if (index !== ordersListIndex) {
        this.showOrderContentsHeight[index] = false;
        setTimeout(() => this.showOrderContentsDisplay[index] = false, 1000);
      }
    });
  }

  /**
   * Event listener that updates the order's contents
   */
  public orderDataChanged(ordersListIndex: number) {
    for (let formOrderKey in this.orderData.value) {
      switch (formOrderKey) {
        case "boxesLoaded":
        case "boxes":
        case "valvesReceived":
        case "valves":
          this.ordersList[ordersListIndex]["orderData"]["hasMerch"][formOrderKey] = this.orderData.get(formOrderKey).value;
          this.ordersListChange.emit(this.ordersList);

          break;
        case "invoice":
          this.ordersList[ordersListIndex]["orderData"][formOrderKey] = this.orderData.get(formOrderKey).value;
          this.ordersListChange.emit(this.ordersList);

          break;
        default:
          this.logger.warn("Route Card component: orderDataChanged(): An unknown formControl was processed");

          break;
      }
    }
  }

  /**
   * Adds or subtracts one from a given form field
   * 
   * @param field The form field that's getting tallied
   * @param direction "up" | "down"
   */
  public tally(field: string, direction: string) {
    // Tally down one
    if (direction === "down" && this.orderData.get(field).value > 0)
      this.orderData.get(field).setValue(this.orderData.get(field).value - 1);
      
    // Tally up one
    if (direction === "up" && this.orderData.get(field).value < 99)
      this.orderData.get(field).setValue(this.orderData.get(field).value + 1);
  }

  public async modifyOrderEntry(orderListIndex: number) {
    this.logger.info("Modifying order entry: ", orderListIndex, this.ordersList[orderListIndex]);

    const modal = await this.modalController.create({
      component: ManualEntryPage,
      componentProps: {
        'modifyOrder': this.ordersList[orderListIndex]
      }
    });

    modal.onDidDismiss().then((dataReturned) => {
      if (dataReturned.data !== undefined) {
        this.ordersList.splice(orderListIndex, 1, dataReturned.data);
        this.changeDetectRef.detectChanges();
        this.ordersListChange.emit(this.ordersList);
      }
    });

    return await modal.present();
  }

  public deleteOrderEntry(orderListIndex: number) {
    this.logger.info("Deleting order entry: ", orderListIndex, this.ordersList[orderListIndex]);
    
    // Removes the deleted entry, but keeps the users selected order expanded open
    this.showOrderContentsHeight.splice(orderListIndex, 1);
    this.showOrderContentsDisplay.splice(orderListIndex, 1);
    // Removes the selected order from the global list
    this.ordersList.splice(orderListIndex, 1);
    this.ordersListChange.emit(this.ordersList);
  }
}