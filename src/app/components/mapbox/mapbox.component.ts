// Angular
import { HttpClient } from '@angular/common/http';
import { Component, ElementRef, EventEmitter, Input, IterableDiffer, OnInit, Output, ViewChild, ChangeDetectorRef, IterableDiffers,
   SimpleChange, IterableChanges, KeyValueDiffer, KeyValueDiffers, KeyValueChanges, IterableChangeRecord } from '@angular/core';

import { Geolocation } from '@capacitor/geolocation';
import { NGXLogger } from 'ngx-logger';
import { Insomnia } from '@awesome-cordova-plugins/insomnia/ngx';

import { Global, AppSettings, OrderStatus, ListMode } from 'src/global';

// Mapbox GL JS
import { Expression, LngLat, LngLatBounds, Map, MapboxOptions, Marker, MarkerOptions } from 'mapbox-gl';
import * as MapboxTraffic from '@mapbox/mapbox-gl-traffic';
import * as Polyline from '@mapbox/polyline';
import * as mapboxgl from 'mapbox-gl';

@Component({
  selector: 'app-mapbox',
  templateUrl: './mapbox.component.html',
  styleUrls: ['./mapbox.component.scss'],
})
export class MapboxComponent implements OnInit {

  // List of orders
  @Input('orders') ordersList: object[] = [];
  private ordersListDiff: IterableDiffer<object>;
  @Output('ordersChange') ordersListChange = new EventEmitter<object[]>();

  // Determines if the map should show location markers or a route
  @Input() listMode: ListMode;
  @Output('listModeChange') listModeChange = new EventEmitter<ListMode>();
  private resumeNavigation: boolean = false;

  // Navigation Data
  @Input() navigationData: object = {};
  @Output('navDataChange') navigationDataChange = new EventEmitter<object>();

  private mapSettings;
  private mapSettingsDiff: KeyValueDiffer<string, any>;

  constructor(
    private global: Global,
    private logger: NGXLogger,
    private insomnia: Insomnia,
    private iterableDiffers: IterableDiffers,
    private keyValueDiffers: KeyValueDiffers,
    private changeDetectRef: ChangeDetectorRef,
    private httpClient: HttpClient
  ) { 
    this.logger.info("MapBox component was used");
  }
  
  public isAPILoaded: boolean = false;

  @ViewChild('ordersMapContainer') ordersMapContainer: ElementRef;
  
  public ordersMapWidth: number = 100;
  public ordersMapHeight: number = 100;

  private ordersMap: Map;
  private ordersMapOptions: MapboxOptions = {
    accessToken: this.global.API_KEYS.MAPBOX.DEFAULT,
    container: "ordersMap",
    style: "mapbox://styles/another-developer/cl0ejhnx8002l14lem7293nw8",
    center: new LngLat(-122.91881224688939, 45.555767250595046),
    zoom: 7,
    maxZoom: 17,
    maxPitch: (Number(this.global.appSettings.mapSettings.rendering.dimensions) === 2) ?
                /* 2D */ 0 : 80 /* 3D */,
    maxBounds: new LngLatBounds(
      new LngLat(-125.32882278575109, 41.99833101033676), // SW
      new LngLat(-117.03227563302825, 48.99923113904174) // NE
    )
  };
  private mapStyles: {[k: string]: string} = {
    default: "mapbox://styles/another-developer/cl0ejhnx8002l14lem7293nw8",
    satellite: "mapbox://styles/another-developer/cl0fx9xop000914qdqs6t123m"
  };
  private trafficLayer = new MapboxTraffic({ showTrafficButton: false });

  // Map Markers
  private businessMarkersList: Marker[] = [];
  private markerOptions: MarkerOptions = {
    draggable: false,
    element: this.markerLabelFactory() 
  };
  
  public markerLabelFactory(label?: number | string, color?: string): HTMLElement {
    if (typeof label === "number") label++;

    const mapMarkerSVG = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
    mapMarkerSVG.setAttribute("display", "block");
    mapMarkerSVG.setAttribute("height", "41px");
    mapMarkerSVG.setAttribute("width", "27px");
    mapMarkerSVG.setAttribute("viewBox", "0 0 27 41");
    mapMarkerSVG.setAttribute("style", "top: -14.75px;");

    const definitions = document.createElementNS('http://www.w3.org/2000/svg', 'defs');
    mapMarkerSVG.appendChild(definitions);

    const radialGradient = document.createElementNS('http://www.w3.org/2000/svg', 'radialGradient');
    radialGradient.setAttribute("id", "shadowGradient");
    radialGradient.innerHTML = '<stop offset="10%" stop-opacity="0.4"></stop><stop offset="100%" stop-opacity="0.05"></stop>';
    definitions.appendChild(radialGradient);

    const markerShadow = document.createElementNS('http://www.w3.org/2000/svg', 'ellipse');
    markerShadow.setAttribute("cx", "13.5");
    markerShadow.setAttribute("cy", "34.8");
    markerShadow.setAttribute("rx", "10.5");
    markerShadow.setAttribute("ry", "5.25");
    markerShadow.setAttribute("fill", "url(#shadowGradient)");
    mapMarkerSVG.appendChild(markerShadow);

    const eyeDropMarker = document.createElementNS('http://www.w3.org/2000/svg', 'path');
    eyeDropMarker.setAttribute("fill", color ? color : "#ff0000");
    eyeDropMarker.setAttribute("d", "M27,13.5C27,19.07 20.25,27 14.75,34.5C14.02,35.5 12.98,35.5 12.25,34.5C6.75,27 0,19.22 0,13.5C0,6.04 6.04,0 13.5,0C20.96,0 27,6.04 27,13.5Z");
    mapMarkerSVG.appendChild(eyeDropMarker);

    const eyeDropMarkerOutline =  document.createElementNS('http://www.w3.org/2000/svg', 'path');
    eyeDropMarkerOutline.setAttribute("opacity", "0.25");
    eyeDropMarkerOutline.setAttribute("d", "M13.5,0C6.04,0 0,6.04 0,13.5C0,19.22 6.75,27 12.25,34.5C13,35.52 14.02,35.5 14.75,34.5C20.25,27 27,19.07 27,13.5C27,6.04 20.96,0 13.5,0ZM13.5,1C20.42,1 26,6.58 26,13.5C26,15.9 24.5,19.18 22.22,22.74C19.95,26.3 16.71,30.14 13.94,33.91C13.74,34.18 13.61,34.32 13.5,34.44C13.39,34.32 13.26,34.18 13.06,33.91C10.28,30.13 7.41,26.31 5.02,22.77C2.62,19.23 1,15.95 1,13.5C1,6.58 6.58,1 13.5,1Z");
    mapMarkerSVG.appendChild(eyeDropMarkerOutline);

    const markerCircle = document.createElementNS('http://www.w3.org/2000/svg', 'circle');
    markerCircle.setAttribute("fill", "white");
    markerCircle.setAttribute("cx", "13.5");
    markerCircle.setAttribute("cy", "13.5");
    markerCircle.setAttribute("r", label ? "10" : "5.5");
    mapMarkerSVG.appendChild(markerCircle);
    
    if (label) {
      const markerLabel = document.createElementNS('http://www.w3.org/2000/svg', 'text');
      markerLabel.setAttribute("x", "50%");
      markerLabel.setAttribute("y", "17");
      markerLabel.setAttribute("text-anchor", "middle");
      markerLabel.innerHTML = String(label);
      mapMarkerSVG.appendChild(markerLabel);
    }

    return mapMarkerSVG as unknown as HTMLElement;
  };

  // Longbottom HQ Marker
  private longbottomMarker: Marker = new Marker({
    draggable: false,
    element: this.markerLabelFactory("LB", "#23414c")

  }).setLngLat(new LngLat(-122.9189784459558, 45.55564729429608));
  private longbottomWarehouseDoor: LngLat = new LngLat(-122.91934841801599, 45.555346901534655);

  // Direction API related
  private isGeneratingOptimizedRoute: boolean = false; // Used to prevent change detection from running "order moved" code
  public directionsResults$;

  public ngOnInit() {
    this.ordersListDiff = this.iterableDiffers.find(this.ordersList).create();
    this.mapSettingsDiff = this.keyValueDiffers.find(this.global.appSettings.mapSettings).create();

    // Wait for the map object to be initialized
    this.initOrdersMap()
      .then(() => {
        // Wait for map to have completed loading
        this.ordersMap.on("load", () => {
          // If app was relaunched while navigating, resume navigation
          if (this.listMode === ListMode.NAVIGATE) {
            // TODO: Check with user if they have delivered any orders in since app relaunch
            
            this.longbottomMarker.addTo(this.ordersMap);

            this.resumeNavigation = true;
            this.generateRoute();
          }
        });
      });
  }

  public ngDoCheck() {
    const ordersListChanges: IterableChanges<object> = this.ordersListDiff.diff(this.ordersList);
    const mapSettingsChanges: KeyValueChanges<string, any> = this.mapSettingsDiff.diff(this.global.appSettings.mapSettings);

    // Apply changes made in the app settings for the map
    if (mapSettingsChanges) {
      this.mapSettings = this.global.appSettings.mapSettings;

      mapSettingsChanges.forEachChangedItem((changes) => {
        if (changes.key === "rendering") {
          this.ordersMap.setStyle(this.mapStyles[changes.currentValue.terrainType]);
        }

        if (changes.key === "showTraffic") {
            this.trafficLayer.options.showTraffic = changes.currentValue;
            this.trafficLayer.render();
        }
      });
    }

    // If an order gets added, changed, moved, or deleted
    if (this.isAPILoaded && ordersListChanges !== null) {
      this.ordersListModified(ordersListChanges);
    }
  }

  public ngOnChanges(changes: SimpleChange) {
    if (changes["listMode"]) {
      switch (changes["listMode"].currentValue) {
        case "": break;
        case "modify":
          this.longbottomMarker.remove();
          this.setRouteVisibility("route", false);

          break;
        case "reorder":
          this.longbottomMarker.addTo(this.ordersMap);

          // An array of coordinates that includes all business and Longbottom's
          const allMarkers: LngLat[] = [];

          // Add all business marker coordinates to allMarkers array
          for (let markerIndex in this.businessMarkersList) allMarkers.push(this.businessMarkersList[markerIndex].getLngLat());
          // Add Longbottom Warehouse coordinates to allMarkers array
          allMarkers.push(this.longbottomMarker.getLngLat());

          this.fitMarkerBounds(500, this.ordersMap, allMarkers);
          
          this.generateRoute(this.global.appSettings["mapSettings"]["optimizeRoute"]);
          this.setRouteVisibility("route", true);

          break;
        case "navigate":
          // If previousValue is undefined. App was relaunched and ngOnInit is starting navigation.
          if (!changes["listMode"].previousValue) break;

          for (const orderIndex in this.ordersList) {
            this.ordersList[orderIndex]["metadata"]["delivery_status"] = OrderStatus.PENDING;
          }

          this.ordersListChange.emit(this.ordersList);

          this.initNavigation();

          break;
        default:
          this.logger.warn(`MapBox component: ngOnChanges(): Unknown listMode "${changes["listMode"]}" was processed`);

          break;
      }
    }
  }
  
  public ngAfterViewChecked() {
    // If both elements exist
    if (this.ordersMapContainer && this.ordersMap) {

      // If the map width is not the same as the container and container is larger than 0, then set map width
      if (this.ordersMapWidth !== this.ordersMapContainer.nativeElement.offsetWidth && this.ordersMapContainer.nativeElement.offsetWidth > 0) {
        this.ordersMapWidth = this.ordersMapContainer.nativeElement.offsetWidth;
        this.changeDetectRef.detectChanges();
      }
      
      // If the map height is not the same as the container and container is larger than 0, then set map height
      if (this.ordersMapHeight !== this.ordersMapContainer.nativeElement.offsetHeight && this.ordersMapContainer.nativeElement.offsetHeight > 0) {
        this.ordersMapHeight = this.ordersMapContainer.nativeElement.offsetHeight;
        this.changeDetectRef.detectChanges();
      }

      this.ordersMap.resize();
    }
  }

  private async initOrdersMap(): Promise<any> {
    this.logger.trace("MapBox component: initOrdersMap() was invoked");

    this.ordersMap = new Map(this.ordersMapOptions);
    this.mapSettings = this.global.appSettings.mapSettings;
    
    // Add Traffic Layer plugin to map
    this.ordersMap.addControl(this.trafficLayer);
    
    // Event handler for when a style has done loading. So that settings that mutate the style can be applied
    this.ordersMap.on("styledata", () => {
      // Apply traffic visibility setting
      this.trafficLayer.options.showTraffic = this.mapSettings.showTraffic;
      this.trafficLayer.render();
    });

    // Apply map style settings
    this.ordersMap.setStyle(this.mapStyles[this.mapSettings.rendering.terrainType]);

    // When the map has finished loading
    this.ordersMap.on("load", () => {
      this.logger.trace("MapBox component: Mapbox map successfully loaded");

      this.isAPILoaded = true;

      this.initMapMarkers();
    });

    this.ordersMap.on('error', (error: mapboxgl.ErrorEvent) => {
      this.logger.error("MapBox component: An error occurred trying to load the map");
      this.logger.error(error.error.stack);
    });

    return;
  }

  /**
   * ordersList array may already have orders in it before the Mapbox API has loaded.
   * Called to check if all orders business objects have coordinates, and adds them to the map.
   */
  private initMapMarkers() {
    this.logger.trace("MapBox component: initMapMarkers() was invoked");

    if (this.businessMarkersList.length !== this.ordersList.length) {
      for (let orderIndex in this.ordersList) {
        this.addBusinessMarker(Number(orderIndex));
      }

      this.fitMarkerBounds(2000);}
  }

  /**
   * A change handler that determines the type of change, and responds appropriately
   */
   private ordersListModified(ordersListChanges: IterableChanges<object>) {
    let recordList: Array<object> = [];
    let isOrderAdded = false;
    let isOrderRemoved = false;
    let isOrderMoved = false;

    if (ordersListChanges) {
      this.logger.debug("MapBox component: ordersList has been updated");
      
      // Order was added or modified
      ordersListChanges.forEachAddedItem((recordAdded: IterableChangeRecord<object>) => {
        isOrderAdded = true;
        recordList.push({ recordType: "added", recordData: recordAdded });
      });

      // Order was moved or removed
      ordersListChanges.forEachMovedItem((recordMoved: IterableChangeRecord<object>) => {
        isOrderMoved = true;
        recordList.push({ recordType: "moved", recordData: recordMoved });
      });

      // Order was removed or modified
      ordersListChanges.forEachRemovedItem((recordRemoved: IterableChangeRecord<object>) => {
        isOrderRemoved = true;
        recordList.push({ recordType: "removed", recordData: recordRemoved });
      });

      // Order was added
      if (isOrderAdded && !isOrderRemoved) {
        this.logger.debug("MapBox component: Order(s) has/have been added");

        recordList.forEach((record) => {
          if (record["recordType"] === "added") this.addBusinessMarker(record["recordData"]["currentIndex"]);
        });
        
        this.fitMarkerBounds(1000);
      }
      
      // Order was modified
      if (isOrderAdded && isOrderRemoved) {
        this.logger.debug("MapBox component: An order was modified");
        this.logger.error("MapBox component: AN ORDER WAS MODIFIED. CHECK IF ANYTHING NEEDS TO BE DONE.");

        // This scenario was previously only used to detect if the address or business name was modified
      }

      // Order was removed
      if (!isOrderAdded && isOrderRemoved) {
        this.logger.debug("MapBox component: An order was deleted");

        recordList.forEach((record) => {
          if (record["recordType"] === "removed") {
            // Remove the deleted order's marker from the map
            this.businessMarkersList[record["recordData"]["previousIndex"]].remove();

            // Remove the deleted order from the markers list
            this.businessMarkersList.splice(record["recordData"]["previousIndex"], 1);
          }
        });
        
        this.fitMarkerBounds(750);
      }

      // Order was moved
      if (isOrderMoved && !isOrderRemoved) {
        if (!this.isGeneratingOptimizedRoute) {
          this.logger.debug("MapBox component: An order was moved");
        
          // The first record is the order that's being moved
          const record = recordList[0];
          // Relocate the moved marker
          this.relocateBusinessMarker(
           record["recordData"]["currentIndex"],
           record["recordData"]["previousIndex"]);
          // Update route directions
          this.generateRoute(false);
        } else { console.warn("// FIXME: This might be taking the markers and orders out of sync");
          this.isGeneratingOptimizedRoute = false;
          // Refresh all markers?
        }
      }
    }
  }

  /**
   * Adds a marker to the map, using an order from ordersList
   * 
   * @param ordersListIndex Index of order in ordersList to add in the marker's array
   */
   private addBusinessMarker(ordersListIndex: number) {
    this.logger.trace("MapBox component: addBusinessMarker() was invoked");

    const businessCoordinates = this.ordersList[ordersListIndex]["business"]["mapData"]["coords"];
    const businessMarkerOptions = this.markerOptions;
    businessMarkerOptions.element = this.markerLabelFactory(ordersListIndex);

    const businessMarker: Marker = new Marker(businessMarkerOptions)
      .setLngLat(new LngLat(businessCoordinates.lng, businessCoordinates.lat))
      .addTo(this.ordersMap);

    this.businessMarkersList.push(businessMarker);
  }

  /**
   * Moves marker to a new index in businessMarkersList array, and updates all marker labels to their new index positions
   * 
   * @param currentIndex The new index where the marker will be placed
   * @param previousIndex The marker that's to be moved
   */
   private relocateBusinessMarker(currentIndex: number, previousIndex?: number) {
    this.logger.trace("MapBox component: relocateBusinessMarker() was invoked");

    // Removes the order's marker that's being moved, and saves into a variable
    const movedMarker = this.businessMarkersList.splice(previousIndex, 1);

    // Then inserts the saved marker into its new position
    this.businessMarkersList.splice(currentIndex, 0, movedMarker[0]);

    // Iterates over all markers to update their labels to match their index position
    this.businessMarkersList.forEach((marker, markerIndex) => {
      const businessMarkerOptions = this.markerOptions;
      businessMarkerOptions.element = this.markerLabelFactory(markerIndex);

      this.businessMarkersList[markerIndex].remove();
      this.businessMarkersList[markerIndex] = new Marker(businessMarkerOptions)
        .setLngLat(marker.getLngLat())
        .addTo(this.ordersMap);
    });
  }

  /**
   * Updates the marker to have the same position and map coordinates as the order with same index in ordersList
   * 
   * @param ordersListIndex Index of order in ordersList to refresh in the marker's array
   */
  private refreshBusinessMarker(ordersListIndex: number) {
    this.logger.trace("MapBox component: refreshBusinessMarker() was invoked");

    const businessCoordinates = this.ordersList[ordersListIndex]["business"]["mapData"]["coords"];

    const businessMarkerOptions = this.markerOptions;
    businessMarkerOptions.element = this.markerLabelFactory(ordersListIndex);

    const businessMarker: Marker = new Marker(this.markerOptions)
      .setLngLat(new LngLat(businessCoordinates.lng, businessCoordinates.lat))
      .addTo(this.ordersMap);

    this.businessMarkersList.splice(ordersListIndex, 1, businessMarker);
  }

  /**
   * Calculates min & max values for longitude & latitude, and then asks 
   * the MapBox API to get the closest zoom level to those coordinates
   * 
   * @param timeout Delay in milliseconds before panning to new location
   * @param ordersMap The map that's being addressed
   * @param fitToCoords An array of coordinates to zoom the map view to
   */
   private fitMarkerBounds(timeout: number, ordersMap?: Map, fitToCoords?: LngLat[]) {
    this.logger.trace("MapBox component: fitMarkerBounds() was invoked");

    setTimeout(() => {
      let north: number, east: number, south: number, west: number;

      if (fitToCoords) {
        for (let coords of fitToCoords) {
          const latitude = coords.lat;
          const longitude = coords.lng;

          if (!north) {
            // Sets initial cords to test against
            north = latitude;   east = longitude;
            south = latitude;   west = longitude;
          } else {
            // Finds range for latitude
            if (latitude > north) north = latitude;
            if (latitude < south) south = latitude;
    
            // Finds range for longitude
            if (longitude < west) west = longitude;
            if (longitude > east) east = longitude;
          }
        }
      } else {
        for (let listIndex in this.businessMarkersList) {
          const latitude = this.businessMarkersList[listIndex].getLngLat().lat;
          const longitude = this.businessMarkersList[listIndex].getLngLat().lng;
    
          if (listIndex === '0') {
            // Sets initial cords to test against
            north = latitude;   east = longitude;
            south = latitude;   west = longitude;
          } else {
            // Finds range for latitude
            if (latitude > north) north = latitude;
            if (latitude < south) south = latitude;
    
            // Finds range for longitude
            if (longitude < west) west = longitude;
            if (longitude > east) east = longitude;
          }
        }
      }
  
      if (!ordersMap) ordersMap = this.ordersMap;
  
      if (this.businessMarkersList.length > 0) {
        const markerBounds = new LngLatBounds(
          new LngLat(west, south),
          new LngLat(east, north)
        );

        ordersMap.fitBounds(markerBounds, { padding: 41 });
      } else if (this.businessMarkersList.length === 0) {
        ordersMap.setZoom(this.ordersMapOptions.zoom);
        ordersMap.panTo(new LngLat(-122.91881224688939, 45.555767250595046));
      }
    }, timeout);
  }

  /**
   * Update layer visibility property
   * 
   * @param layer ID of the layer that's being addressed
   * @param visible Boolean to determine if to set layer as visible or not
   */
  private setRouteVisibility(layer: string, visible: boolean) {
    // Stop execution of function if Mapbox hasn't initialized yet
    if (!this.ordersMap) return;
    
    this.logger.trace("MapBox component: setRouteVisibility() was invoked");

    switch (layer) {
      case "route":
        if (this.ordersMap.getLayer("routeInner")) {
          this.ordersMap.setLayoutProperty("routeInner", "visibility", visible ? "visible" : "none");
          this.ordersMap.setLayoutProperty("routeOutline", "visibility", visible ? "visible" : "none");
        }

        break;
      case "legs":
        console.log(this.directionsResults$);

        break;
      default:
        this.logger.warn(`routeLayerHandler(): An unknown layer "${layer}" was provided`);

        break;
    }
  }

  /**
   * // TODO: Add function description 
   * 
   * @param layer "route" | "leg" | "step"
   * @param index 
   */
  private setRoutePath(layer: string, index?: number) {
    this.logger.trace("MapBox component: setRoutePath() was invoked");

    // Adds and updates path
    if (layer === "route") {
      const routeCoordinates = Polyline.toGeoJSON(this.directionsResults$["routes"][0]["geometry"]);
            
      if (this.ordersMap.getSource("route")) {
        // Updates the route source data
        ///@ts-ignore
        this.ordersMap.getSource("route").setData({ type: 'Feature', properties: {}, geometry: routeCoordinates });
      } else {
        // Else adds a source with the coordinates of the path
        this.ordersMap.addSource("route", { type: "geojson", data: { type: 'Feature', properties: {}, geometry: routeCoordinates }});

        // And layers that show the path
        this.ordersMap.addLayer({
          id: 'routeInner', type: 'line', source: 'route',
          layout: { visibility: 'visible', 'line-join': 'round', 'line-cap': 'round' },
          paint: { 'line-color': '#999', 'line-width': 6 }
        }).moveLayer("routeInner", "road-intersection");
    
        this.ordersMap.addLayer({
          id: 'routeOutline', type: 'line', source: 'route',
          layout: { visibility: 'visible', 'line-join': 'round', 'line-cap': 'round' },
          paint: { 'line-color': '#666', 'line-width': 2, 'line-gap-width': 6 }
        }).moveLayer("routeOutline", "road-intersection");
      }
    }

    // Adds and updates route leg path
    if (layer === "legs") {
      const routeLegSteps = this.directionsResults$["routes"][0]["legs"][index]["steps"];
      let routeLegCoordinates: [number, number][] = [];

      routeLegSteps.forEach(step => {
        // Decode polyline string into coordinates array
        const decodedPolyline = Polyline.decode(step["geometry"]);
        
        // Push each coordinate into routeLegCoordinates while converting them from LatLng into LngLat
        if (decodedPolyline) decodedPolyline.forEach((coords) => routeLegCoordinates.push( [coords[1], coords[0]] ));
      });

      if (this.ordersMap.getSource("routeLeg")) {
        // Updates the route source data
        ///@ts-ignore
        this.ordersMap.getSource("routeLeg").setData({ type: 'Feature', properties: {}, geometry: 
          { type: "LineString", coordinates: routeLegCoordinates }
        });
      } else {
        // Else adds a source with the coordinates of the path
        this.ordersMap.addSource("routeLeg", { type: "geojson", data: { type: 'Feature', properties: {}, geometry: 
          { type: "LineString", coordinates: routeLegCoordinates }
        }});

        // And layers that show the path
        this.ordersMap.addLayer({
          id: 'routeLegInner', type: 'line', source: 'routeLeg',
          layout: { visibility: 'visible', 'line-join': 'round', 'line-cap': 'round' },
          paint: { 'line-color': '#3880ff', 'line-width': 6 }
        }).moveLayer("routeLegInner", "road-intersection");

        // Hide full route path inner behind current route leg
        this.ordersMap.moveLayer("routeInner", "routeLegInner");
        // Set full route path to be partially transparent
        this.ordersMap.setPaintProperty("routeInner", "line-opacity", 0.5);
    
        this.ordersMap.addLayer({
          id: 'routeLegOutline', type: 'line', source: 'routeLeg',
          layout: { visibility: 'visible', 'line-join': 'round', 'line-cap': 'round' },
          paint: { 'line-color': '#5260ff', 'line-width': 2, 'line-gap-width': 6 }
        }).moveLayer("routeLegOutline", "road-intersection");

        // Hide full route path outline behind current route leg
        this.ordersMap.moveLayer("routeOutline", "routeLegOutline");
        // Set full route path to be partially transparent
        this.ordersMap.setPaintProperty("routeOutline", "line-opacity", 0.5);
      }
    }

    // Adds and updates route leg step path
    if (layer === "step") {
      const routeLegStep = this.directionsResults$["routes"][0]["legs"][this.currentRouteLeg]["steps"][index]["geometry"];
      let routeStepCoordinates: [number, number][] = 
         routeLegStep ? Polyline.decode(routeLegStep) : undefined;

      // If the route step failed to return coordinates, stop execution of adding polyline to the map
      if (!routeStepCoordinates) {
        this.logger.trace("Couldn't fetch polyline for a route step");

        return;
      }

      let convertedCoordinates: [number, number][] = [];

      // Rebuild routeStepCoordinates to Mapbox's coordinate type standards
      routeStepCoordinates.forEach((coords) => convertedCoordinates.push( [coords[1], coords[0]] ));
      routeStepCoordinates = convertedCoordinates;

      if (this.ordersMap.getSource("routeStep")) {
        // Updates the route source data
        ///@ts-ignore
        this.ordersMap.getSource("routeStep").setData({ type: 'Feature', properties: {}, geometry: 
          { type: "LineString", coordinates: routeStepCoordinates }
        });
      } else {
        // Else adds a source with the coordinates of the path
        this.ordersMap.addSource("routeStep", { type: "geojson", data: { type: 'Feature', properties: {}, geometry: 
          { type: "LineString", coordinates: routeStepCoordinates }
        }});

        // And layers that show the path
        this.ordersMap.addLayer({
          id: 'routeStepInner', type: 'line', source: 'routeStep',
          layout: { visibility: 'visible', 'line-join': 'round', 'line-cap': 'round' },
          paint: { 'line-color': '#f55', 'line-width': 6 }
        }).moveLayer("routeStepInner", "road-intersection");

        // Hide route leg path behind step path
        this.ordersMap.moveLayer("routeLegInner", "routeStepInner");
    
        this.ordersMap.addLayer({
          id: 'routeStepOutline', type: 'line', source: 'routeStep',
          layout: { visibility: 'visible', 'line-join': 'round', 'line-cap': 'round' },
          paint: { 'line-color': '#f00', 'line-width': 2, 'line-gap-width': 6 }
        }).moveLayer("routeStepOutline", "road-intersection");

        // Hide route leg path behind step path
        this.ordersMap.moveLayer("routeLegOutline", "routeStepOutline");
      }
    }
  }

  /**
   * Generates a route 
   * 
   * @param optimizeWaypoints A boolean for having the route re-order/optimized
   */
  private generateRoute(optimizeWaypoints?: boolean) {
    this.logger.trace("MapBox component: generateRoute() was invoked");

    // TODO: Have the server do directions. For ...
    //    - Getting all the bells and whistles of the Directions API
    //    - Route optimization, because Optimization API accepts less coords than Directions API
    //    - Controlling quota ... Token API?
    //    - Preferred and operational time waypoint settings
    //    - User specific location delivery duration

    if (!optimizeWaypoints) optimizeWaypoints = false;

    // Init departureTime to 9:30 AM
    let departureTime = new Date();
    departureTime.setHours(9, 30);

    // If it's past 9:30 AM set departure time to current time
    if (departureTime.getTime() < Date.now()) 
      departureTime = new Date();
    
    const convertLngLatToString = (coords: LngLat): string => `${coords.lng},${coords.lat}`;
    let directionWaypoints: string;

    const updateLocationRequest = this.getCurrentLocation()
      .then((geoLocation: GeolocationPosition) => {
        // If not resuming navigation
        if (!this.resumeNavigation) {
          // Set warehouse as origin
          directionWaypoints = convertLngLatToString(this.longbottomWarehouseDoor) + ";";
          // Add all businesses as waypoints
          this.businessMarkersList.forEach((marker) => directionWaypoints += (convertLngLatToString(marker.getLngLat()) + ";"));
          // Set warehouse as destination
          directionWaypoints += convertLngLatToString(this.longbottomWarehouseDoor);
        } 
        // Resuming navigation
        else {
          // Set user's current location as the origin
          directionWaypoints = `${geoLocation.coords.longitude},${geoLocation.coords.latitude};`;
          // Add all businesses that haven't been completed as waypoints
          for (const orderIndex in this.ordersList) {
            if (this.ordersList[orderIndex]["metadata"]["delivery_status"] < OrderStatus.COMPLETED) {
              const orderCoords = this.ordersList[orderIndex]["business"]["mapData"]["coords"];

              directionWaypoints += `${orderCoords["lng"]},${orderCoords["lat"]};`;
            }
          }
          // Set warehouse as destination
          directionWaypoints += convertLngLatToString(this.longbottomWarehouseDoor);
        }
      })
      .catch((error) => {
        this.logger.error("Mapbox Component: generateRoute(): There was a problem getting the user's current location");
        this.logger.error(error);
      });

    // Wait for users location to be found and directionsWaypoints to be initialized
    updateLocationRequest.finally(() => {
      // https://docs.mapbox.com/api/navigation/directions/#retrieve-directions
      const directionsAPI_REST_call =
       `https://api.mapbox.com/directions/v5/mapbox/driving-traffic/${directionWaypoints}?` +
       `geometries=polyline&overview=full&steps=true&depart_at=${departureTime.toISOString().replace(/.\d+.\d+Z$/g, "")}&access_token=${this.global.API_KEYS.MAPBOX.DEFAULT}`;
      
      this.httpClient.get(directionsAPI_REST_call)
      .subscribe(
        (response) => {
          if (response["code"] === "Ok") {
            this.directionsResults$ = response;
            
            this.navigationData = response;
            this.navigationDataChange.emit(this.navigationData);
  
            this.setRoutePath("route");
  
            if (this.resumeNavigation) this.initNavigation();
          } else 
            this.logger.error(`Directions returned with status of ${response["code"]}`);
        },
        (error) => {
          this.logger.error("Mapbox Component: There was a problem retrieving directions");
          this.logger.error(error);
        },
        () => {}
      );
    });
    
    // TODO: Route optimization ... after moving the above logic server-side?
    /*let waypoint_order: number[];*/
    /*this.directionsResults$ = this.mapDirectionsService.route(request).pipe(map((response) => {
      if (optimizeWaypoints) {
        if (response.status === "OK") {
          if (response.result["status"] === "OK") {
            this.isGeneratingOptimizedRoute = true;

            waypoint_order = response.result.routes[0].waypoint_order;
            const tempOrdersList: object[] = [];
            const tempMarkersList: object[] = [];

            // Adds order and marker to its new index position in the temporary arrays
            for (let orderIndex in this.ordersList){
              tempOrdersList[orderIndex] = this.ordersList[waypoint_order[orderIndex]];
              tempMarkersList[orderIndex] = this.businessMarkersList[waypoint_order[orderIndex]];
            }

            // Apply the temporary arrays
            for (let orderIndex in this.ordersList) {
              this.ordersList[orderIndex] = tempOrdersList[orderIndex];
              this.businessMarkersList[orderIndex] = tempMarkersList[orderIndex];
            }

            // Updates all marker labels
            this.ordersList.forEach((data, orderIndex) => {
              this.refreshBusinessMarker(orderIndex);
            });
          } else {
            this.logger.error("Google Maps component: generateRoute(): There was a problem with the route generation");
            this.logger.error(response.result);
          }
        } else {
          this.logger.error("Google Maps component: generateRoute(): There was a problem with the request");
          this.logger.error(response);
        }
      }

      this.navigationData = response.result.routes[0].legs;
      this.navigationDataChange.emit(this.navigationData);

      return response.result;
    }));*/
  }

  private refreshTime: number = 0;

  // From business to business
  private currentRouteLeg: number = 0;
  // From route turn to turn
  private currentRouteStep: number;
  // Closest progressing coordinate
  private stepPathIndex: number;
  private nearestPolylineIndex: number = 0;
  private reachedDestination: boolean = false;
  private orderConfirmationRequired: boolean = true;

  private locationBoundaries = {
    start: 250,
    end: 250,
    atDestination: 100
  };
  // TODO: Show accuracy only when it's more than 50 meters
  
  // TODO: Figure out if I'll need this for the van model
  private currentVanPosition = {
    visible: false,
    position: this.longbottomWarehouseDoor,
    heading: 0,
    scale: 5 // TODO: Implement scaling of van relative to zoom
  };
  
  // DELETE: This currentLocationMarker when done debugging
  public currentLocationMarker: Marker;

  // FIXME: Return trip to Longbottom Warehouse doesn't have an ordersList object,
  //        so Navigation Card doesn't know when to progress into next stages 
  //        ... could wait an elapsed time (maybe 30 secs), or give it it's own object
  private initNavigation() {
    this.logger.trace("MapBox component: initNavigation() was invoked");

    this.insomnia.keepAwake()
      .then(() => {
        this.logger.warn("Mapbox Component: initNavigation(): Screen insomnia mode successfully enabled! >:D");
      })
      .catch((error) => {
        this.logger.warn("Mapbox Component: initNavigation(): Was unable to enable screen insomnia");
        this.logger.warn(error);
      });

    this.setNavigationArrow(this.longbottomWarehouseDoor, 50);

    this.loadRouteLeg();

    // Set refresh time after start loop, so that the above logic runs first
    this.refreshTime = 5000; //300000; // 5 Minutes
    
    this.startLoop();

    // Set initial navigation mode zoom
    setTimeout(() => this.ordersMap.flyTo({ zoom: 15.5 }), this.refreshTime * 2.5);
        
    // TODO: Open file to see list
    // Navigate button should open the route in the user's selected navigation app
    //    Draw over app plugin and/or navigation launcher plugin

    /* What should happen if the user doesn't start near the start place? */
    //    Check with the user what stops have been completed

    // Get updated directions?

    // What if the user wants/needs to change the order of their route while its in-progress?

  }

  /**
  * // TODO: Write function description
  */
  private startLoop() {
    this.updateCurrentLocation().then((currentLocation: GeolocationPosition) => {
      const startLocationCoords = this.navigationData["waypoints"][this.currentRouteLeg]["location"];

      const distanceFromStart =
        new LngLat(startLocationCoords[0], startLocationCoords[1])
          .distanceTo(new LngLat(currentLocation.coords.longitude, currentLocation.coords.latitude));

      if (this.locationBoundaries.start >= distanceFromStart) {
          setTimeout(() => this.startLoop(), this.refreshTime);
        } else {
          this.logger.info("MapBox component: startLoop(): Starting route segment navigation");

          this.currentVanPosition.visible = true;
          
          if (this.currentRouteLeg < (this.navigationData["routes"][0]["legs"].length - 1)) {
            this.ordersList[this.currentRouteLeg]["metadata"]["delivery_status"] = OrderStatus.IN_PROGRESS;
            this.global.saveOrdersList();
            this.ordersListChange.emit(this.ordersList);
          }

          this.navigationLoop();
        }
    });
  }

  /**
   * // TODO: Write function description
   */
   private navigationLoop() {
    // TODO: Calculate refresh time based n location accuracy, distance from end of step, travel duration, and speed
    // currentLocation.coords.accuracy is a value that describes accuracy in meters
    // this.navigationData[this.currentRouteSegment]["end_location"]
    // this.navigationData[this.currentRouteSegment].duration.value is duration in seconds that the routeSegment is expected to take
    // Set camera bounds to current routeSegmentStep

    // 1 sec = 1000 millisec
    // 1 mph = 0.44704 m/s

    this.refreshTime = 250;

    this.updateCurrentLocation().then((currentLocation: GeolocationPosition) => {
      const endLocationCoords = this.navigationData["waypoints"][this.currentRouteLeg + 1]["location"];

      const distanceFromEnd =
        new LngLat(endLocationCoords[0], endLocationCoords[1])
          .distanceTo(new LngLat(currentLocation.coords.longitude, currentLocation.coords.latitude));

      if (this.locationBoundaries.end < distanceFromEnd) {
        setTimeout(() => this.navigationLoop(), this.refreshTime);
      } else {
        this.logger.info("MapBox component: navigationLoop(): Ending segment");
        
        setTimeout(() => this.endLoop(), this.refreshTime);
      }
    });
  }
  
  /**
   * // TODO: Write function description
   */
  private endLoop() {
    this.refreshTime = 1000;

    this.updateCurrentLocation().then((currentLocation: GeolocationPosition) => {
      setTimeout(() => {
        // TODO: Record how long a driver has remained in the delivery zone and distance from destination

        const endLocationCoords = this.navigationData["waypoints"][this.currentRouteLeg + 1]["location"];

        const distanceDestination = 
          new LngLat(endLocationCoords[0], endLocationCoords[1])
            .distanceTo(new LngLat(currentLocation.coords.longitude, currentLocation.coords.latitude));

        // Determine if we're still delivery or are done for the day
        if (this.currentRouteLeg < (this.navigationData["routes"][0]["legs"].length - 1)) {
          // TODO: Determine if user has stopped before marking as complete or warning
          // Determine based on speed spiced up with accuracy (walking speed is less than 2m/s)
          // and wait time (stoplights take 90s)

          // If user leaves atDestinationCircle or moves fast enough
          if (this.reachedDestination && (distanceDestination > this.locationBoundaries.atDestination || currentLocation.coords.speed > 4.20)) {
            // Check if the order requires attention
            if (this.orderConfirmationRequired) {
              // TODO: Warn user
              console.warn("Not so quick there buddy!");
            } else {
              this.ordersList[this.currentRouteLeg]["metadata"]["delivery_status"] = OrderStatus.COMPLETED;
            }
          }

          // If user enters atDestinationCircle for the first time
          if (!this.reachedDestination && distanceDestination < this.locationBoundaries.atDestination) {
            this.logger.info("MapBox component: endLoop(): Entered atDestination distance");

            this.reachedDestination = true;

            this.ordersList[this.currentRouteLeg]["metadata"]["delivery_status"] = OrderStatus.AT_DESTINATION;
            this.global.saveOrdersList();
            this.ordersListChange.emit(this.ordersList);
          }

          // Check if all prerequisites have been met
          if (this.ordersList[this.currentRouteLeg]["metadata"]["delivery_status"] === OrderStatus.COMPLETED) {
            // Progress to next order
            this.global.saveOrdersList();

            // Log the processed GPS accuracy info
            this.logger.info(this.global.appOpStats.geolocation.accuracy.navigation.processData());
    
            this.reachedDestination = false;
            this.nearestPolylineIndex = 0;
            this.currentRouteStep = 0;
            this.stepPathIndex = 0;
            this.currentRouteLeg++;
            this.loadRouteLeg();

            this.startLoop();
          } else {
            this.endLoop();
          }
        } else {
          this.logger.info("MapBox component: endLoop(): Driver has finished their route.");

          this.listMode = ListMode.COMPLETED;
          this.listModeChange.emit(this.listMode);

          this.insomnia.allowSleepAgain()
            .then(() => {
              this.logger.warn("Mapbox Component: endLoop(): Screen insomnia was disabled successfully.");
            })
            .catch(() => {
              this.logger.warn("Mapbox Component: endLoop(): Hey, you deliver coffee. How bad can it be being indefinitely awake?");
            });
        }
      }, this.refreshTime);
    });
  }

  private calcMapboxCircleRadius(meters: number): Expression { return ["interpolate", ["exponential", 2], ["zoom"], 0, 0, 17, (meters * 2.18603) ] };

  /**
   * Adds and updates the navigation arrow(s) on the map
   * 
   * @param currentLocation Location coordinates
   * @param heading Degrees away from North
   * @param accuracy The radius of meters where something is believed to be
   */
  private setNavigationArrow(currentLocation: LngLat, accuracy: number) {
    this.logger.trace("MapBox component: setNavigationArrow() was invoked");

    // FIXME: Updating navigation arrow / van location is jaggered, not smooth

    if (Number(this.global.appSettings.mapSettings.rendering.dimensions) === 2) {
      if (!this.ordersMap.getSource("navigationArrow")) {
        /*const navigationArrowSVG =
      `<svg viewBox="0 0 128 128" xmlns="http://www.w3.org/2000/svg">
          <defs></defs>
          <ellipse style="fill: rgb(255, 255, 255);" cx="64" cy="64" rx="64" ry="64"></ellipse>
          <path style="fill: #3880ff;" d="M 60.42,39.16 Q64,32 67,39.16 L96,96 L64,80 L32,96 L64,32 Z"></path>
        </svg>`

        // Did not work
        //const navigationArrowBlob = new Blob([navigationArrowSVG], {type: 'image/svg+xml'});
        //const navigationArrowURL = URL.createObjectURL(navigationArrowBlob);
        //const navigationArrow = new Image(512, 512);
        //navigationArrow.src = "https://cdn-icons-png.flaticon.com/512/63/63059.png";
        //navigationArrow.addEventListener("load", () => URL.revokeObjectURL(navigationArrowURL), {once: true});

        //this.ordersMap.addImage("navigationArrowIMG", navigationArrow);

        Try by rendering in a canvas and pulling image data
        https://stackoverflow.com/questions/3768565/drawing-an-svg-file-on-a-html5-canvas
        https://developer.mozilla.org/en-US/docs/Web/API/CanvasRenderingContext2D/getImageData

        This is in attempt to dynamically mutate the navigation arrow and do things like set its color
        ... which can be more easily done with an .SDF image
        
        Could I do something like this?
        Map.prototype.addImage = (name: string) => {

        };*/

        this.ordersMap.addSource("navigationArrow", {
          type: "geojson", data: { type: "FeatureCollection", features: [
              { type: "Feature", properties: {}, geometry: { type: "Point", coordinates: currentLocation.toArray()} }
            ]
          }
        });

        this.ordersMap.addLayer({
          id: "navigationArrowMarker", type: "symbol", source: "navigationArrow",
          layout: { "icon-image": "navigationArrow", "icon-size": 0.25 }
        });

        this.ordersMap.addLayer({
          id: "navigationArrowAccuracy", type: "circle", source: "navigationArrow",
          paint: { "circle-opacity": 0.1, "circle-radius": this.calcMapboxCircleRadius(accuracy)
            // Pixels to meters conversion by zoom level at lat 40.
            // Meters per px = 59959.436 meters * (1/2)^zoom
          }
        }).moveLayer("navigationArrowAccuracy", "navigationArrowMarker");
      } else {
        // Update the location of the navigation arrow
        ///@ts-ignore
        this.ordersMap.getSource("navigationArrow").setData(
            {type: "Feature", properties: {}, geometry: { type: "Point", coordinates: currentLocation.toArray() } }
          );

        // Sets the accuracy circle its radius for where something is believed to be
        this.ordersMap.setPaintProperty("navigationArrowAccuracy", "circle-radius", this.calcMapboxCircleRadius(accuracy));
      }
    }

    if (Number(this.global.appSettings.mapSettings.rendering.dimensions) === 3) {
      // TODO: 3D navigation arrow
      // Might just use the 2D and have it extrude ... which probably isn't possible
    }
  }

  /**
   * // TODO: Write function description
   */
  private loadRouteLeg() {
    this.logger.trace("MapBox component: loadRouteLeg() was invoked");

    // Determine if the order needs to be approved to be considered fully delivered
    if (this.currentRouteLeg < (this.navigationData["routes"][0]["legs"].length - 1)) {
      const orderData = this.ordersList[this.currentRouteLeg]["orderData"];
      
      if (orderData["paymentType"] === 'cod' || orderData["posters"] || orderData["pickUps"]) {
        this.orderConfirmationRequired = true;
      } else {
        this.orderConfirmationRequired = false;
      }
    }

    // Displays polyline for the current route leg
    this.setRoutePath("legs", this.currentRouteLeg);

    const navDataWaypoints = this.navigationData["waypoints"];

    // TODO: Turn boundary and accuracy circles in to 3D models and maybe add force-field like boundary wall 
    //        Keep google.maps.Circle for 2D mode
    // Sets the boundary circles that define where navigation start and stop to or from a location
    if (this.currentRouteLeg === 0) {
      this.ordersMap.addSource("startCircle", { type: "geojson", data: { 
          type: 'Feature', properties: {}, geometry: { type: "Point", coordinates: navDataWaypoints[this.currentRouteLeg]["location"] }}})
        .addLayer({ id: "startCircle", type: "circle", source: "startCircle", 
          paint: { "circle-color": "#F00", "circle-opacity": 0.15, 
            "circle-stroke-color": "#F00", "circle-stroke-opacity": 0.5, "circle-stroke-width": 2,
            "circle-radius": this.calcMapboxCircleRadius(this.locationBoundaries.start)
          }
        }).moveLayer("startCircle", "navigationArrowAccuracy");

      this.ordersMap.addSource("endCircle", { type: "geojson", data: { 
          type: 'Feature', properties: {}, geometry: { type: "Point", coordinates: navDataWaypoints[this.currentRouteLeg + 1]["location"] }}})
        .addLayer({ id: "endCircle", type: "circle", source: "endCircle", 
          paint: { "circle-color": "#F00", "circle-opacity": 0.15, 
            "circle-stroke-color": "#F00", "circle-stroke-opacity": 0.5, "circle-stroke-width": 2,
            "circle-radius": this.calcMapboxCircleRadius(this.locationBoundaries.end)
          }
        }).moveLayer("endCircle", "navigationArrowAccuracy");

      this.ordersMap.addSource("atDestinationCircle", { type: "geojson", data: { 
          type: 'Feature', properties: {}, geometry: { type: "Point", coordinates: navDataWaypoints[this.currentRouteLeg + 1]["location"] }}})
        .addLayer({ id: "atDestinationCircle", type: "circle", source: "atDestinationCircle", 
          paint: { "circle-color": "#F00", "circle-opacity": 0.15, 
            "circle-stroke-color": "#F00", "circle-stroke-opacity": 0.5, "circle-stroke-width": 2,
            "circle-radius": this.calcMapboxCircleRadius(this.locationBoundaries.atDestination)
          }
        }).moveLayer("atDestinationCircle", "navigationArrowAccuracy");
    } else {
      // Updates the start circle location
      ///@ts-ignore
      this.ordersMap.getSource("startCircle").setData(
          {type: "Feature", properties: {}, geometry: { type: "Point", coordinates: navDataWaypoints[this.currentRouteLeg]["location"] } }
        );
        
      // Updates the start circle location
      ///@ts-ignore
      this.ordersMap.getSource("endCircle").setData(
        {type: "Feature", properties: {}, geometry: { type: "Point", coordinates: navDataWaypoints[this.currentRouteLeg + 1]["location"] } }
      );
      
      // Updates the start circle location
      ///@ts-ignore
      this.ordersMap.getSource("atDestinationCircle").setData(
        {type: "Feature", properties: {}, geometry: { type: "Point", coordinates: navDataWaypoints[this.currentRouteLeg + 1]["location"] } }
      );
    }
    
    // Zooms and moves towards the current section of the route
    this.fitMarkerBounds(500, this.ordersMap, 
      [new LngLat(navDataWaypoints[this.currentRouteLeg]["location"][0], navDataWaypoints[this.currentRouteLeg]["location"][1]),
      new LngLat(navDataWaypoints[this.currentRouteLeg + 1]["location"][0], navDataWaypoints[this.currentRouteLeg + 1]["location"][1])]
    );
  }

  /**
   * Snaps currentVanPositionMarker to route
   */
  private updateCurrentMapPosition(currentLocation: GeolocationPosition) {
    // IDEAS: Open file to view
    //    If user left startCircle, zoom camera to bounds of current step of the segment, show directions on navigation card
    //    Remember closest polylineIndex and check within certain index bound difference based on this.refreshTime and distance
    //      along the polyline the user could have traveled 
    //      (this.refreshTime * currentLocation.coords.speed) < first coord in direction of travel that exceeds
    //    Save routeSegmentStep in navigationData as currentStep

    // If location accuracy is within a "some distance" meter radius, then snap navigation arrow to route
    if (currentLocation.coords.accuracy < 0) {
      const routeLegPolyline = this.ordersMap.getSource("routeLeg")["_data"]["geometry"]["coordinates"];
      let previousPolylineDistance: number;
      let pI = this.nearestPolylineIndex - 2;

      if (pI < 0) pI = 0;

      // Find the closest polylineIndex to currentLocation
      for (pI; pI < (routeLegPolyline.length - 1); pI++) {
        if (typeof previousPolylineDistance === "undefined") {
          previousPolylineDistance = 
            new LngLat(currentLocation.coords.longitude, currentLocation.coords.latitude)
              .distanceTo(new LngLat(routeLegPolyline[pI][0], routeLegPolyline[pI][1]));
        } else {
          const currentPolylineDistance = 
            new LngLat(currentLocation.coords.longitude, currentLocation.coords.latitude)
              .distanceTo(new LngLat(routeLegPolyline[pI][0], routeLegPolyline[pI][1]));

          if (currentPolylineDistance < previousPolylineDistance) {
            // Hypothetically getting closer to current location
            previousPolylineDistance = currentPolylineDistance;
          } else {
            if (previousPolylineDistance < currentPolylineDistance) {
              this.nearestPolylineIndex = Number(pI) - 1;

              this.currentVanPosition.position = new LngLat(routeLegPolyline[this.nearestPolylineIndex][0], routeLegPolyline[this.nearestPolylineIndex][1]);
              this.currentVanPosition.heading = this.getHeading(
                new LngLat(routeLegPolyline[this.nearestPolylineIndex][0], routeLegPolyline[this.nearestPolylineIndex][1]),
                new LngLat(routeLegPolyline[this.nearestPolylineIndex + 1][0], routeLegPolyline[this.nearestPolylineIndex + 1][1])
              );

              // TODO: Allow to snap between two coordinates
                // Only snap to route if it's within currentLocation.coords.accuracy, else loop through all of routeSegmentPolyline to find closest

              break;
            }
          }
        }
      }

      let stepPolylineStartIndex: number = 0;

      // Determine routeSegmentStep and stepPathIndex based off of nearestPolylineIdex
      for (let stepIndex in this.navigationData["routes"][0]["legs"][this.currentRouteLeg]["steps"]) {
        const stepPolyline = Polyline.decode(this.navigationData["routes"][0]["legs"][this.currentRouteLeg]["steps"][stepIndex]["geometry"]);

        let stepPolylineEndIndex = stepPolylineStartIndex + stepPolyline.length;

        if (this.nearestPolylineIndex > stepPolylineStartIndex && this.nearestPolylineIndex < stepPolylineEndIndex) {
          this.stepPathIndex = this.nearestPolylineIndex - stepPolylineStartIndex;

          // If current route step changes update it
          if (Number(stepIndex) !== this.navigationData["routes"][0]["legs"][this.currentRouteLeg]["current_step"]) {
            this.currentRouteStep = Number(stepIndex);
            this.setRoutePath("step", this.currentRouteStep);

            this.navigationData["routes"][0]["legs"][this.currentRouteLeg]["current_step"] = this.currentRouteStep ;
            this.navigationDataChange.emit(this.navigationData);
          }

          // TODO: De-snap from  polyline if user gets to far from route
          // TODO: Calculate how much distance of the route has been completed? and set pathIndex
            // Keep a copy of the pI prior to updating. Saving resources, yeah!

          break;
        } else {
          stepPolylineStartIndex = stepPolylineEndIndex;
        }

      }
    } else {
      // Set it to what it is
      this.currentVanPosition.position = new LngLat(currentLocation.coords.longitude, currentLocation.coords.latitude);
      this.currentVanPosition.heading = currentLocation.coords.heading | 0;
    }

    // Update the navigation arrow / van
    this.setNavigationArrow(this.currentVanPosition.position, currentLocation.coords.accuracy);

    // TODO: Set zoom based on speed
    // TODO: Stop moving camera if user zooms or moves view away from current location
    this.ordersMap.flyTo({
      center: this.currentVanPosition.position,
      // Following zoom must be a 13 or greater
      zoom: this.ordersMap.getZoom() > 13 ? this.ordersMap.getZoom() : 13,
      bearing: this.currentVanPosition.heading, // TODO: Set heading based on user preference (Ex. snap to north or current heading)
      pitch: this.ordersMap.getPitch(),

      screenSpeed: (1 / (this.refreshTime / 1000)) // TODO: Calculate flyTo screenSpeed using refreshTime
    });
  }

  /**
   * Handler function that fetches location data from getCurrentLocation() function,
   * and calls updateCurrentMapPosition() function to update navigation arrow / van.
   * 
   * @returns GeolocationPosition object
   */
  private updateCurrentLocation = async () => {
    return await this.getCurrentLocation().then((geoLocation: GeolocationPosition) => {

      console.timeEnd("gpsResolve");

      let currentLocation: GeolocationPosition;

      if (this.global.__isProduction) {
        currentLocation = geoLocation;
      } else {
        if (typeof this.currentLocationMarker === "undefined") {
          this.currentLocationMarker = new Marker({
            draggable: true,
            element: this.markerLabelFactory(null, "#FF9C33")
          });
          this.currentLocationMarker.setLngLat(this.longbottomWarehouseDoor);
          this.currentLocationMarker.addTo(this.ordersMap);
        }
  
        let coordinates = this.currentLocationMarker.getLngLat();
  
        currentLocation = {
          coords: {
            accuracy: Math.round(Math.random() * 500),
            latitude: coordinates.lat,
            longitude: coordinates.lng,
            altitude: null,
            altitudeAccuracy: null,
            heading: null,
            speed: null
          },
          timestamp: geoLocation.timestamp
        };
      }

      // Track GPS accuracy values
      this.global.appOpStats.geolocation.accuracy.navigation.data.push(currentLocation.coords.accuracy);

      this.updateCurrentMapPosition(currentLocation);

      return currentLocation;
    });
  }

  private getHeading(coords1: LngLat, coords2: LngLat): number {
    // Converts from degrees to radians.
    function toRadians(degrees) {
      return degrees * Math.PI / 180;
    };
     
    // Converts from radians to degrees.
    function toDegrees(radians) {
      return radians * 180 / Math.PI;
    }

    // https://cdn.jsdelivr.net/npm/geodesy@2/latlon-spherical.js ... function rhumbBearingTo()
    const π = Math.PI;
    const φ1 = toRadians(coords1.lat);
    const φ2 = toRadians(coords2.lat);
    let Δλ = toRadians(coords2.lng - coords1.lng);
    // if dLon over 180° take shorter rhumb line across the anti-meridian:
    if (Math.abs(Δλ) > π) Δλ = Δλ > 0 ? -(2 * π - Δλ) : (2 * π + Δλ);

    const Δψ = Math.log(Math.tan(φ2 / 2 + π / 4) / Math.tan(φ1 / 2 + π / 4));

    const θ = Math.atan2(Δλ, Δψ);

    const bearing = toDegrees(θ);

    if ( 0 <= bearing && bearing < 360) return bearing;

    // https://cdn.jsdelivr.net/npm/geodesy@2/dms.js ... function wrap360()
    const x = bearing, a = 180, p = 360;
    return (((2*a*x/p)%p)+p)%p;
  }

  /**
   * Checks permission and fetches the user's current location
   * 
   * @returns Promise that resolves with type Position
   */
  private getCurrentLocation = async () => {
    const currentLocation  = await Geolocation.checkPermissions()
    .then(async (status) => {
      // Only checking fine GPS location permissions
      switch (status.location) {
        case "granted":
          console.time("gpsResolve");

          return await Geolocation.getCurrentPosition(
            /*{
              enableHighAccuracy: true,
              timeout: 10000,//this.refreshTime ? this.refreshTime : 10000,
              maximumAge: 5000
            }*/
          );

        case "denied":
        case "prompt":
        case "prompt-with-rationale":
          this.listMode = ListMode.REORDER;
          this.listModeChange.emit(this.listMode);

          await Geolocation.requestPermissions();
          
          return null;
      }

    })
    .catch(error => {
      this.logger.error("MapBox component: getCurrentLocation():", error);
    });

    return currentLocation;
  }
}