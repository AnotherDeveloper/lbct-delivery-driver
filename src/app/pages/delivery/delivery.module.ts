import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientJsonpModule, HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { IonicModule } from '@ionic/angular';

import { DeliveryPageRoutingModule } from './delivery-routing.module';

import { DeliveryPage } from './delivery.page';
import { OrdersCardComponent } from 'src/app/components/orders-card/orders-card.component';
import { MapboxComponent } from 'src/app/components/mapbox/mapbox.component';
import { NavigationCardComponent } from 'src/app/components/navigation-card/navigation-card.component';
import { ManualEntryPageModule } from './modals/manual-entry/manual-entry.module';

import { AuthInterceptor } from 'src/app/services/interceptors.service';

@NgModule({
  imports: [
    CommonModule,
    IonicModule,
    ReactiveFormsModule,
    FormsModule,
    DeliveryPageRoutingModule,
    ManualEntryPageModule,
    HttpClientModule,
    HttpClientJsonpModule
  ],
  declarations: [DeliveryPage, OrdersCardComponent, MapboxComponent, NavigationCardComponent],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true }
  ]
})
export class DeliveryPageModule {}
