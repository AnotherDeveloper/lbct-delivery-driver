import { OrderStatus } from './../../../global';
import { Component, IterableChanges, IterableDiffer, IterableDiffers, OnInit, ViewChild, ElementRef, SimpleChanges } from '@angular/core';
import { ModalController } from '@ionic/angular';
import * as confetti from 'canvas-confetti';
import { NGXLogger } from 'ngx-logger';
import { Global, ListMode } from 'src/global';

import { ManualEntryPage } from './modals/manual-entry/manual-entry.page';

@Component({
  selector: 'app-delivery',
  templateUrl: './delivery.page.html',
  styleUrls: ['./delivery.page.scss']
})
export class DeliveryPage implements OnInit {

  // Tells Orders Card and Mapbox components how to display and process data
  public listMode: ListMode = undefined;

  public isFooterHidden = false;

  // Check routingDisabled statements
  public ordersList: Array<Object> = [];
  private ordersListDiff: IterableDiffer<object>;

  public navigationData: object;

  public routingDisabled: boolean = true;

  constructor(
    private global: Global,
    private modalController: ModalController,
    private logger: NGXLogger,
    private iterableDiffers: IterableDiffers,
  ) {
    this.logger.info("Delivery page was opened");

    this.ordersList = global.ordersList;

    this.checkOrdersListStatus();
  }

  public ngOnInit() {
    // Create a change tracker for ordersList
    this.ordersListDiff = this.iterableDiffers.find(this.ordersList).create();
  }

  public ngAfterViewInit() {
    // Check if there are any orders
    if (this.ordersList.length > 0) {
      // Loop through all orders to check if an order has "delivery_status" key in its metadata.
      // If one does, normally all will.
      for (let orderIndex in this.ordersList) {
        const deliveryStatus = this.ordersList[orderIndex]["metadata"]["delivery_status"];

        // If a "delivery_status" key is present, and the first order is greater than PENDING, but all are less than COMPLETED.
        // Then the user was still navigating before relaunch
        if (typeof deliveryStatus !== "undefined" && deliveryStatus < OrderStatus.COMPLETED) {          
          this.startNavigation();

          this.logger.info("Resuming navigation");

          break;
          
        }
      }
    } 
    
    if (typeof this.listMode === "undefined" || this.listMode.length === 0) {
      this.listMode = ListMode.MODIFY;
    }
  }

  public ngDoCheck() {
    const ordersListChanges: IterableChanges<object> = this.ordersListDiff.diff(this.ordersList);

    if (ordersListChanges) {
      let isStatusComplete = true;

      for (let orderIndex in this.ordersList) {
        if (this.ordersList[orderIndex]["metadata"]["status"] !== "complete")
          isStatusComplete = false;
      }

      if (isStatusComplete) this.routingDisabled = false;
      else this.routingDisabled = true;
    }
  }

  public syncOrdersList(newOrdersList: object[]) {
    this.ordersList = newOrdersList;

    this.checkOrdersListStatus();
  }

  public syncNavigationData(newNavigationData: object) {
    this.navigationData = newNavigationData;
  }

  public syncListMode(listMode: ListMode) {
    this.listMode = listMode;

    if (this.listMode === ListMode.COMPLETED)
      this.completedRoute();
  }

  public async openModalManualEntry() {
    this.logger.info("Creating new order");

    const modal = await this.modalController.create({
      component: ManualEntryPage
    });

    modal.onDidDismiss().then((dataReturned) => {
      if (dataReturned.data) {
        this.ordersList.push(dataReturned.data);
      }
    });

    return await modal.present();
  }
  
  private checkOrdersListStatus() {
    for (let ordersListIndex in this.ordersList) {
      if (this.ordersList[ordersListIndex]["metadata"]) {
        if (this.ordersList[ordersListIndex]["metadata"]["status"] !== "complete") {
          this.routingDisabled = true;
  
          break;
        }
  
        if ((Number.parseInt(ordersListIndex) + 1) === this.ordersList.length) this.routingDisabled = false;
      }
    }
  }

  public continueToRouting() {
    this.logger.info("Changing to routing mode");

    this.listMode = ListMode.REORDER;
    
    this.global.saveOrdersList();
  }

  public returnToOrderEntry() {
    this.logger.info("Changing to entry mode");

    this.listMode = ListMode.MODIFY;

    this.global.saveOrdersList();
  }

  public startNavigation() {
    this.logger.info("Starting navigation");

    this.listMode = ListMode.NAVIGATE;
    this.isFooterHidden = true;

    this.global.saveOrdersList();

    // TODO: Let user change routing while in navigation
  }

  @ViewChild('confettiCanvas') confettiCanvas: ElementRef;

  private completedRoute() {
    this.isFooterHidden = true;

    this.global.ordersList = [];
    this.global.saveOrdersList();

    setTimeout(() => {
      const wohoo = confetti.create(this.confettiCanvas.nativeElement, {
        resize: true,
        useWorker: true
      });

      const confettiLoop = () => {
          setTimeout(() => {
            wohoo();
            confettiLoop();
        }, 5000);
      }

      wohoo();
      confettiLoop();
    }, 1000);
  }
  
  /* // TODO: Refine and complete this silliness :P *confetti*
      https://www.npmjs.com/package/canvas-confetti

      Could empty ordersList and put an object in for return navigation stages and determining if it's next day
  */
}