import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ManualEntryPage } from './manual-entry.page';

describe('ManualEntryPage', () => {
  let component: ManualEntryPage;
  let fixture: ComponentFixture<ManualEntryPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManualEntryPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ManualEntryPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
