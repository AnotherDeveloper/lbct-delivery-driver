import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ManualEntryPage } from './manual-entry.page';

const routes: Routes = [
  {
    path: '',
    component: ManualEntryPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ManualEntryPageRoutingModule {}
