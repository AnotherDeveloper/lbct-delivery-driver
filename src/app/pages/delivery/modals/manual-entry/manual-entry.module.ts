import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ManualEntryPageRoutingModule } from './manual-entry-routing.module';

import { ManualEntryPage } from './manual-entry.page';
import { MatchingBusinessesComponent } from 'src/app/components/matching-businesses/matching-businesses.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    ManualEntryPageRoutingModule
  ],
  declarations: [ManualEntryPage, MatchingBusinessesComponent]
})
export class ManualEntryPageModule {}
