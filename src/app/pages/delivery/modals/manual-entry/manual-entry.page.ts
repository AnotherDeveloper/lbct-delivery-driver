import { Component, Input, OnInit } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';

import { ModalController, ToastController } from '@ionic/angular';

import { NGXLogger } from 'ngx-logger';
import { debounceTime } from 'rxjs/operators';

import { Global } from 'src/global';

@Component({
  selector: 'app-manual-entry',
  templateUrl: './manual-entry.page.html',
  styleUrls: ['./manual-entry.page.scss'],
})
export class ManualEntryPage implements OnInit {
  // An element attribute that is used when an order needs to be passed through to be edited
  // ngOnInit checks if it's used and takes its data to set the form fields values
  @Input() modifyOrder: object;

  private businessValidator: ValidatorFn = (control: AbstractControl): ValidationErrors | null => {
    const fields = ["name", "address", "address2", "city", "state", "zip"];
    const validationErrorsArray = [];

    for (const i in fields) {
      const formControl = control.get(fields[i]);

      const validationError = {[fields[i]]: formControl.errors};

      if (formControl.errors !== null) {
        validationErrorsArray.push(validationError);
      }
    }

    return validationErrorsArray.length > 0 ? {fieldErrors: validationErrorsArray} : null;
  };

  private orderValidator: ValidatorFn = (control: AbstractControl): ValidationErrors | null => {
    const paymentType = control.get("paymentType");
    const hasMerch = control.get("hasMerch");
    const customerCare = control.get("customerCare");
    const posters = control.get("posters");
    const invoice = control.get("invoice");

    // At least one field, besides paymentType, has to be filled out
    if (hasMerch.value || customerCare.value || posters.value || invoice.value) {
      // paymentType is required if there is merchandise to be delivered
      if ( (hasMerch.value || invoice.value) && paymentType.value === "" ) {
        return { "incomplete": "payment" };
      } else {
        // Everything is a-okay
        return null;
      }
    } else {
      // orderData form is incomplete
      return { "incomplete": "all" };
    }
  };

  // Input fields
  public business = new FormGroup ({
    name: new FormControl('', Validators.required),
    address: new FormControl('', Validators.required),
    address2: new FormControl(''),
    city: new FormControl('', Validators.required),
    state: new FormControl('', [Validators.required, Validators.pattern('[a-zA-Z]{2}')]),
    zip: new FormControl('', [Validators.required, Validators.pattern('^[0-9]{5}(?:-[0-9]{4})?$')])// FIXME: iOS autofill adds a space at the end, ignore spaces
  }, { validators: this.businessValidator });
  public orderData = new FormGroup({
    paymentType: new FormControl(''),
    hasMerch: new FormControl(false),
      valves: new FormControl(false),
    customerCare: new FormControl(false),
    posters: new FormControl(false),
    invoice: new FormControl(false),
    comments: new FormControl('')
  }, { validators: this.orderValidator });

  constructor(
    private modalController: ModalController,
    private toastController: ToastController,
    private httpClient: HttpClient,
    private logger: NGXLogger,
    private global: Global
  ) {
    this.logger.info("Manual Entry modal was opened");
  }

  public ngOnInit() {
    if (this.modifyOrder) {
      this.logger.debug("An order is being modified");

      this.submitButtonText = "Edit Order";

      // Set selectedBusiness to the orders business, so that change detection on order submission can do its thing
      this.selectedBusiness = this.modifyOrder['business']; 

      const modifyBusiness = this.modifyOrder['business'];

      // Populate all the business form fields
      for (let businessKey in modifyBusiness) {
        if (this.business.get(businessKey))
          this.business.get(businessKey).setValue(modifyBusiness[businessKey]);
      }

      const modifyOrderData = this.modifyOrder['orderData'];

      // Populate all the order form fields
      for (let orderDataKey in modifyOrderData) {
        if (this.orderData.get(orderDataKey))
          this.orderData.get(orderDataKey).setValue(modifyOrderData[orderDataKey]);
      }
    } else {
      this.logger.debug("modifyOrder is unused");
      
      this.submitButtonText = "Add Order";
    }

    this.businessDataChangeListener();
  }

  public matchingBusinessesList: object[];
  public matchListLocation: string = "none";
  private selectedBusiness: object;

  private businessDataChangeListener() {
    let prevBusinessData: object;
    prevBusinessData = this.business.value;

    this.business.valueChanges
      .pipe(
        debounceTime(500)
      )
      .subscribe((businessData) => {
        this.global.searchForBusiness(businessData)
          .then(queryResults => {
            // Makes fetched results publicly available
            this.matchingBusinessesList = queryResults;

            // Are all fields fully matched to a business
            let isBusinessMatched = false;

            for (let businessKey in businessData) {
              // Determine what section of the business FormGroup the user has focus
              for (let prevBusinessKey in prevBusinessData) {
                if (businessKey === prevBusinessKey) {
                  switch (businessKey) {
                    case "name":
                      if (businessData[businessKey].length !== prevBusinessData[prevBusinessKey].length) {
                        this.matchListLocation = "name";
                      }

                      break;
                    case "address":
                    case "address2":
                    case "city":
                    case "state":
                    case "zip":
                      if (businessData[businessKey].length !== prevBusinessData[prevBusinessKey].length) {
                        this.matchListLocation = "address";
                      }

                      break;
                    default:
                      this.logger.warn("Manual Entry: businessDataChangeListener(): Unknown business FormGroup key was processed");

                      break;
                  }
                }
              }

              // Iterate through all fields. If they're all matching there is no to suggest matches
              if (this.matchingBusinessesList.length > 0) {
                for (let matchedBusinessKey in this.matchingBusinessesList[0]["business"]) {
                  if (businessKey === matchedBusinessKey)  {
                    if (this.matchingBusinessesList[0]["business"][matchedBusinessKey] !== businessData[businessKey])
                      isBusinessMatched = true;
                  }
                }
              }
            }

            if (!isBusinessMatched) {
              this.matchListLocation = "none";
            }
            
            // Used to identify what FormControl has focus
            prevBusinessData = businessData;
          });
      });
  }

  /**
   * This function takes a number as the selected index in the matchingBusinessList array.
   * Which is used to get the object in the array that contains the business its details,
   * which are applied to the business form fields.
   * 
   * @param businessListIndex The index of the business in matchingBusinessList{}[]
   */
  public selectedBusinessFunc(businessListIndex) {
    this.business.patchValue(this.matchingBusinessesList[businessListIndex]["business"]);

    this.matchListLocation = "none";

    this.selectedBusiness = this.matchingBusinessesList[businessListIndex]["business"];
  }

  // Used to keep validator from being re-ran
  private paymentTypeRequired = false;

  /**
   * Change listener function for orderData ForumGroup
   */
  public orderDataChanged() {
    const paymentType = this.orderData.get("paymentType");

    this.logger.debug("orderDataChanged() was invoked");

    // paymentType is required if there is merchandise to be delivered
    if ( this.orderData.get("hasMerch").value || this.orderData.get("invoice").value ) {
      // If paymentTypeRequired is false
      if ( !this.paymentTypeRequired ) {
        // Add required validator
        paymentType.setValidators([Validators.required]);
        paymentType.updateValueAndValidity();

        this.paymentTypeRequired = true;

        this.logger.debug("orderDataChanged(): paymentType is now required");
      }
    } else {
      // If paymentTypeRequired is true
      if ( this.paymentTypeRequired ) {
        // Remove required validator
        paymentType.clearValidators();
        paymentType.updateValueAndValidity();

        this.paymentTypeRequired = false;

        this.logger.debug("orderDataChanged: paymentType requirement removed");
      }
    }
  }

   /**
   * Adds or subtracts one from a given form field
   * 
   * @param field The form field that's getting tallied
   * @param direction "up" | "down"
   */
    public tally(field: string, direction: string) {
    // Tally down one
    if (direction === "down" && this.orderData.get(field).value > 0)
      this.orderData.get(field).setValue(this.orderData.get(field).value - 1);
      
    // Tally up one
    if (direction === "up" && this.orderData.get(field).value < 99)
      this.orderData.get(field).setValue(this.orderData.get(field).value + 1);
  }

  // TODO: On business validationError, we may need to be able to change the button to a "yes" and "no".
  private generateErrorToastMessage(validationErrors: Object) { // For say some of the information given was wrong 
    let errorMessageArray: Array<string> = [];          // and potentially correct information was found

    let patternList: Array<string> = [],
        requiredList: Array<string> = [], 
        incompleteList: Array<string> = [];

    // Iterate through form sections. Ex: business, then orderData, then etc...
    // And sorts the error types in to the above lists
    for (const formType in validationErrors) {
      this.logger.debug(formType, " : ", validationErrors[formType]);

      // Identify what form section that's being currently being processed
      switch (formType) {
        case "business":
          // Iterate through the form sections fields that returned errors
          for (const fields in validationErrors[formType]) { 
            // Extract the form object out of the given array
            const formFieldObject = validationErrors[formType][fields];

            // Extract the (key-value) / (field Name-errors) from the object that encapsulates it
            for (const formField in formFieldObject) {
              // Iterate through the errors of the forms current field
              for (const formFieldErrors in formFieldObject[formField]) {
                // Identify the type of error
                switch (formFieldErrors) {
                  case "required":
                    requiredList.push(formField);

                    break;
                  case "pattern":
                    patternList.push(formField);

                    break;
                  default:
                    this.logger.warn("generateErrorToastMessage(): Hit default for business switch, formFieldErrors switch");
                    this.logger.warn(formField, " : ", formFieldErrors);

                    errorMessageArray.push(`${formField} : ${formFieldErrors}`);
                    break;
                }
              }
            }
          }

          break;
        case "orderData":
          // Iterate through the form its error objects
          for (const formError in validationErrors[formType]) {
            switch(formError) {
              case "incomplete":
                incompleteList.push(validationErrors[formType][formError]);  

                break;
              default:
                this.logger.warn("generateErrorToastMessage(): Hit default for orderData switch, formError switch");
                this.logger.warn(formError, " : ", validationErrors[formType][formError]);

                errorMessageArray.push(`${formError} : ${validationErrors[formType][formError]}`);
                break;
            }
          }

          break;
        default:
          this.logger.warn("The toaster burnt some toast ...");
          this.logger.warn(formType, " : ", validationErrors[formType]);

          errorMessageArray.push(`${formType} : ${validationErrors[formType]}`);
          break;
      }
    }
    
    if (patternList.length > 0) {
      this.logger.debug("patternList:", patternList);

      // TODO: The last comma needs to be replace with a ", and", or if the list is only two long it needs to be replaced with an "and"
      // Convert array of strings into a single string, and replace all commas to add a space after it
      const patternListString = patternList.toString().replace(/,/g, ", ");
      // Interpolate the above concatenated string into to the error message and determine the proper grammar to be used
      const patternErrorMessage = `The information given in the ${patternListString} ${patternList.length > 1 ? "fields are" : "field is"} invalid.`;

      errorMessageArray.push(patternErrorMessage);
    }

    if (requiredList.length > 0) {
      this.logger.debug("requiredList:", requiredList);

      // TODO: The last comma needs to be replace with a ", and", or if the list is only two long it needs to be replaced with an "and"
      // Convert array of strings into a single string, and replace all commas to add a space after it
      const requiredListString = requiredList.toString().replace(/,/g, ", ");
      // Interpolate the above concatenated string into to the error message and determine the proper grammar to be used
      const requiredErrorMessage = `The ${requiredListString} ${requiredList.length > 1 ? "fields" : "field"} needs to be filled out.`;

      errorMessageArray.push(requiredErrorMessage);
    }

    if (incompleteList.length > 0) {
      this.logger.debug("incompleteList:", incompleteList);
      
      for (const incomplete in incompleteList) {
        switch (incompleteList[incomplete]) {
          case "all":
            errorMessageArray.push("Please fill out what's to be delivered in the order.");

            break;
          case "payment":
            errorMessageArray.push("A payment type needs to be selected.");

            break;
          default:
            this.logger.warn("generateErrorToastMessage(): Hit default for incomplete switch");
            this.logger.warn(incompleteList, " : ", incompleteList[incomplete]);
  
            errorMessageArray.push(`${incompleteList} : ${incompleteList[incomplete]}`);
            break;
        }
      }
    }

    this.logger.debug(errorMessageArray);
    this.showErrorToast(errorMessageArray);
  }

  /**
   * Shows an error message toast and has the user go through them one-by-one
   *  
   * @param errorMessageArray Array of string error messages 
   */
  private async showErrorToast(errorMessageArray: Array<string>) {
    const errorMessage = errorMessageArray.shift();

    this.logger.debug("The displayed errorMessage is: ", errorMessage);
    this.logger.debug("Remaining messages in errorMessageArray: ", errorMessageArray);

    const toast = await this.toastController.create({
      message: errorMessage,
      position: 'bottom',
      buttons: [
        {
          text: 'Ok',
          role: 'cancel',
          handler: () => {
            this.logger.debug("Error message toast was manually closed");
            this.logger.debug(`There are ${errorMessageArray.length} error messages left`);
            
            if (errorMessageArray.length > 0) {
              this.showErrorToast(errorMessageArray);
            }
          }
        }
      ]
    });
    await toast.present();
  }

  submitButtonText: string = "Submit";

  /**
   * // TODO: Write a quick description
   */
  public async submitOrder() {
    // Checks with the Form module if the entered information is valid
    if (this.business.status === "VALID" && this.orderData.status === "VALID") {
      // Creates an object and fetches all information.
      const orderInformation = {
        metadata: { 
          originType: "manual"
        },
        business: this.business.value,
        orderData: this.orderData.value
      };

      // Clear paymentType and set invoice to false if order has no merch
      if (!orderInformation.orderData.hasMerch && ~orderInformation.orderData.invoice) {
        orderInformation.orderData.paymentType = "";
        orderInformation.orderData.invoice = false;
      }
      
      // Determine order status
      if (this.orderData.get("hasMerch").value || this.orderData.get("invoice").value) {
        // If either hasMerch or invoice is false, order is pending completion
        if (!this.orderData.get("hasMerch").value || !this.orderData.get("invoice").value) {
          orderInformation.metadata["status"] = "pending";
        } else {
          // If both hasMerch and invoice are true, and hasMerch is of type boolean,
          // then it's a new order is pending completion
          if (typeof this.orderData.get("hasMerch").value === "boolean")
            orderInformation.metadata["status"] = "pending";
          // If it's an order that is being edited, then hasMerch is of type object, 
          // and we want to leave it alone, as route card's order content pane
          // deals with order status then
        }
      } else {
          // No merchandise or invoices to deal with
          orderInformation.metadata["status"] = "complete";
      }

      // If hasMerch is true, replace it with an object that contains key-value pairs used for tallying
      if (orderInformation.orderData.hasMerch) {
        if (typeof orderInformation.orderData.hasMerch === "boolean") {
          orderInformation.orderData.hasMerch = {
            boxes: 0,
            valves: orderInformation.orderData.valves
          }

          delete orderInformation.orderData.valves;
        }
      } else orderInformation.orderData.hasMerch = false;

      // No need to toss out map or merchandise related data.
      // The Mapbox component handles if something needs to be done with the data or not
      // The Route Card component handles the tally of merchandise
      if (this.modifyOrder) {
        orderInformation.business["mapData"] = this.modifyOrder["business"]["mapData"];

        if (this.orderData.get("hasMerch").value && this.modifyOrder["orderData"]["hasMerch"])
          orderInformation.orderData["hasMerch"] = this.modifyOrder["orderData"]["hasMerch"];
      }

      let selectedBusinessModified = false;

      // Check if a searched business was selected and modified
      if (this.selectedBusiness) {
        for (let formAddressField in orderInformation.business) {
          if (selectedBusinessModified) break;

          for (let selectedAddressField in this.selectedBusiness) {
            if (selectedBusinessModified) break;

            if (formAddressField === selectedAddressField) {
              if (orderInformation.business[formAddressField] !== this.selectedBusiness[selectedAddressField]) {
                selectedBusinessModified = true;
              }
            }
          }
        }

        // If a selected business was not modified, set orderInformation.business equal to it
        // So that key's like "mapData" don't get lost
        if (!selectedBusinessModified) {
          orderInformation.business = this.selectedBusiness;
        }
      }

      // If no searched business was selected (new business) or selected business was modified
      if (!this.selectedBusiness || selectedBusinessModified) {
        // TODO: If selectedBusiness was used, ensure that the placeID is included

        const postContent = {
          requestType: "add_business",
          business: orderInformation.business
        }

        // Sends orderInformation.business to server to be processed (coordinates, operation times), stored, and returned back here
        this.httpClient.post(this.global.__serverDomainName + "/temp", postContent)
          .subscribe(
            (processedBusinessEntry) => {
              // Cache returned business data for later potential use
              this.global.saveBusiness(processedBusinessEntry);

              // Apply the completed full business profile
              orderInformation.business = processedBusinessEntry;

              this.logger.info(`Order has been ${this.modifyOrder ? "modified" : "added"}: `, orderInformation);

              // Dismiss modal, and return the collected information to the page that opened this modal.
              this.modalController.dismiss(orderInformation);
            },
            (error) => {
              this.logger.warn("submitOrder(): There was a problem while sending or processing the new business's information");
              this.logger.warn(error);
            },
            () => {}
          )
      } else { // If a selected / stored business was used
        this.logger.info(`Order has been ${this.modifyOrder ? "modified" : "added"}: `, orderInformation);

        // Dismiss modal, and return the collected information to the page that opened this modal.
        await this.modalController.dismiss(orderInformation);
      }
    } else {
      // If one or both forum segments returns invalid, find out why.

      // TODO: Highlight missing field
      // .item-interactive.ion-(invalid | valid)
      // Apply if a field was reported 'touched: false'
      this.logger.warn('Order couldn\'t be added:', this.business, this.orderData);

      let validationErrors = {};
      
      if (this.business.status === "INVALID") {
        // TODO: Try to assume business if insufficient data was provided, and notify user

        validationErrors["business"] = this.business.errors.fieldErrors;
      }

      if (this.orderData.status === "INVALID") {
        validationErrors["orderData"] = this.orderData.errors;
      }

      this.generateErrorToastMessage(validationErrors);
    }
  }

  public async dismissModal() {
    await this.modalController.dismiss();
  }
}