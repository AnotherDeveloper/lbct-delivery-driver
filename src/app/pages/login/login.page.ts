import { Global } from 'src/global';
import { AuthenticationService } from 'src/app/services/authentication.service';

import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { FormControl, FormGroup, ValidatorFn, AbstractControl, ValidationErrors, Validators } from '@angular/forms';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';

import { IonInput, ToastController } from '@ionic/angular';
import { Geolocation } from '@capacitor/geolocation';

import { FingerprintAIO, FingerprintOptions} from '@awesome-cordova-plugins/fingerprint-aio/ngx';
import { NGXLogger } from 'ngx-logger';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  private credentialsValidator: ValidatorFn = (control: AbstractControl): ValidationErrors | null => {
    const passwordInput = control.get("password");
    const confirmPasswordInput = control.get("confirmPassword");

    const validatorErrors: object = {};

    // When Password input is visible
    if (this.isPasswordVisible) {
      // Then the input is required to be used
      if (!passwordInput.value) {
        validatorErrors["isPassInputRequired"] = true;
  
        passwordInput.setErrors({ isPassInputRequired: true });
      }
    }

    // When Confirm Password input is visible
    if (this.isPassConfirmVisible) {
      // TODO: Main Password input should meet certain criteria

      // Input must match the other Password input
      if (passwordInput.value !== confirmPasswordInput.value) {
        validatorErrors["doPasswordsMatch"] = false;

        confirmPasswordInput.setErrors({ doPasswordsMatch: false });
      }
    }

    return Object.keys(validatorErrors).length ? validatorErrors : null;
  }

  constructor(
    private global: Global,
    private auth: AuthenticationService,
    private httpClient: HttpClient,
    private router: Router,
    private toastController: ToastController,
    private biometricsPlugin: FingerprintAIO,
    private logger: NGXLogger
  ) { }

  public loginForm = new FormGroup({
    employeeID: new FormControl(this.auth.getUserID(), Validators.required),
    password: new FormControl(),
    confirmPassword: new FormControl(),
    useBiometrics: new FormControl(this.global.appSettings["useBiometricLogin"])
  }, {validators: this.credentialsValidator});

  @ViewChild("inputPassword") inputPassword: IonInput;
  @ViewChild("inputConfirmPassword") inputConfirmPassword: IonInput;

  // User Interface visibility
  public isSpinnerVisible: boolean = false;
  public statusText: string = "";

  public isAppLoaded: boolean = false;
  public isInputsVisible: boolean = false;
  public isPasswordVisible: boolean = false;
  public isPassConfirmVisible: boolean = false;
  public showPassword: boolean = false;
  public useBiometricLogin: boolean = false;

  // Footer button action and text
  public buttonActionIndex: number = 0;
  public buttonActionText: string[] = ["Next", "Login"];

  // The user's first ever time logging in to their account
  public isInitialLogin: boolean = false;

  // If the app is running an new version for the first time
  private isNewVersion: boolean = false;

  // TODO: List of things to implement in file
    // Mimic splash screen for background
    // If isInitialLogin, do a tour of the app

  public ngOnInit() {
    this.logger.info("Login page was opened");
    
    this.global.checkAppVersion()
      .then(() => {
        const storedAppV = this.global.__appVersionStored;
        
        // Check if hard coded and stored versions differ
        for (let hardKey in this.global.__appVersion) {
          for (let storedKey in storedAppV) {
            if (hardKey === storedKey) {
              if (this.global.__appVersion[hardKey] !== storedAppV[storedKey]) {
                this.isNewVersion = true;

                this.logger.info(`Login Page: User has upgraded from version: ${storedAppV.major}.${storedAppV.minor}.${storedAppV.patch}`);
              }
            }
          }
        }

        // Check if the app has all needed permissions
        Geolocation.checkPermissions()
          .then((status) => {
            switch(status.location) {
              case "granted":
                this.logger.info("ngOnInit(): Location permission granted");

                break;
              case "denied":
              case "prompt":
              case "prompt-with-rationale":
                this.logger.info("ngOnInit(): Requesting location permission");

                Geolocation.requestPermissions();
                
                break;
            }
          })
          .catch((error) => {
            this.logger.info("ngOnInit(): An error occurred while checking for location permissions");
            this.logger.info(error);
          });

        // If this is not the first time running the current version of this app
        if (!this.isNewVersion) {
          this.statusText = "Checking for updates..."
          this.isSpinnerVisible = true;

          // Check for updates
          this.global.checkForUpdates()
            .then(() => {
              this.isSpinnerVisible = false;
              this.statusText = "";

              this.showLogin();
            });
        } else {
          this.showLogin();
        }
      });
  }

  /**
   * Displays an error toast with the provided message
   * 
   * @param errorMessage 
   */
  private async showErrorToast(errorMessage: string) {
    const errorToast = await this.toastController.create({
      message: errorMessage,
      position: 'bottom',
      color: "danger",
      buttons: [
        {
          text: 'Ok',
          role: 'cancel',
          handler: () => {
            this.logger.debug("Error message toast was manually closed");
          }
        }
      ]
    });

    await errorToast.present();
  }

  /**
   * Toggles variables to change the user interface into a login form, 
   * and check & use fingerprint / face unlocking
   */
  private showLogin() {
    // Causes the user interface to move the business name upwards 
    setTimeout(() => this.isAppLoaded = true, 1000);

    setTimeout(() => {
      // Check that biometric login is enabled
      if (this.global.appSettings["useBiometricLogin"]) {
        this.useBiometricLogin = true;
        this.isPasswordVisible = true;
        this.buttonActionIndex = 1;

        this.showBiometricLogin();
      }

      // If biometrics is not used
      if (!this.global.appSettings["useBiometricLogin"]) {
        // Make sure a full length employeeID is entered/loaded
        if (String(this.loginForm.get("employeeID").value).length === 6) {
          // If token is expired
          if (this.auth.isTokenExpired) {
            // If a user was logged-in in the past, show password field
            this.isPasswordVisible = true;
            this.buttonActionIndex = 1;
          }
        }
      }

      // Causes the user interface to show the login input fields
      this.isInputsVisible = true;
    }, 1500);
  }

  /**
   * Brings up the platforms native biometric unlocking interface
   */
  public showBiometricLogin() {
    // Make sure a full length employeeID is entered/loaded
    if (String(this.loginForm.get("employeeID").value).length === 6) {
      // If token is expired
      if (this.auth.isTokenExpired) {
        // Load password from FingerprintAIO
        this.biometricsPlugin.loadBiometricSecret({
          title: "Unlock",
          subtitle: "Longbottom - Delivery Driver"
        }).then(password => {
          // TODO: Can the returned result not be a password string?
          this.logIn(password);
        }).catch(error => {
          this.logger.warn("ngOnInit(): Could not show FingerprintAIO");
          this.logger.warn(error);

          switch (error.code) {
            case this.biometricsPlugin.BIOMETRIC_DISMISSED:
            case this.biometricsPlugin.BIOMETRIC_PIN_OR_PATTERN_DISMISSED:
              // Ignore prompt dismissal for logging-in
              
              break;
            case this.biometricsPlugin.BIOMETRIC_SECRET_NOT_FOUND:
            case this.biometricsPlugin.BIOMETRIC_NOT_ENROLLED:
              this.loginForm.get("useBiometrics").setValue(false);

              this.global.appSettings["useBiometricLogin"] = false;
              this.global.saveSettings();

              this.showErrorToast("Biometrics isn't setup. Usage has been turned off.");

              break;
            default:
              // TODO: Handle possible errors
              this.showErrorToast("There is a problem loading the biometric login plugin.");

              break;
          }
        });
      }
    }
  }

  private cancelBlurEvent: boolean = false;

  /**
   * An event handler function that checks if the entered employee ID is valid 
   * and forwards the focus to the password input field
   * 
   * @param event ionChange | ionBlur | keyup
   */
  public isEmployeeIDValid(event) {
    const employeeID: string = event.target.value;

    // Reset cancelBlurEvent and stop this function from completing
    if (this.cancelBlurEvent) {
      this.cancelBlurEvent = false;

      return;
    }

    // Cancel ionBlur event to prevent isEmployeeIDValid() from being fired twice
    if (event.type === "keyup" || event.type === "click" || employeeID.length === 6) {
      this.cancelBlurEvent = true;
    }

    // Only continue if a full length employee ID number has been entered
    if (event.type === "ionChange" && employeeID.length < 6)
      return;

    // Allow employee ID's that are under 7 ONLY digits long
    if (employeeID.length <= 6 && /^\d+$/.test(employeeID)) {
      const postContent: object = {
        requestType: "authentication",
        username: employeeID
      };

      // Check with server on the provided employeeID
      this.httpClient.post(this.global.__serverDomainName + "/auth", postContent)
        .subscribe(
          result => {
            if (!result["userFound"]) {
              this.logger.log("User entered an invalid employee ID");

              this.showErrorToast("The entered employee ID doesn't hasn't been registered in the system.");
            } else {
              // Show password field
              this.isPasswordVisible = true;
              this.buttonActionIndex = 1;

              // Show confirm password field if a passwords needs to be registered
              if (result["isInitialLogin"]) {
                // And set isInitialLogin to true to trigger a tour of the app
                this.isInitialLogin = true;
                this.isPassConfirmVisible = true;
              }

              setTimeout(() => this.inputPassword.setFocus(), 500);
            }
          },
          (error: HttpErrorResponse) => {
            this.logger.error("isEmployeeIDValid(): There was a problem communicating with the server");
            this.logger.error(JSON.stringify(error));

            this.showErrorToast("There was a problem communicating with the server.");
          },
          () => {}
        );
    }
  }

  /**
   * Toggle visibility of password input fields
   */
  public togglePasswordVisibility() {
    this.showPassword = !this.showPassword;

    this.inputPassword.type = this.showPassword ? "text" : "password";

    if (this.isPassConfirmVisible)
      this.inputConfirmPassword.type = this.showPassword ? "text" : "password";
  }

  /**
   * Footer button handler for progressing the user in the login process or logging-in
   */
  public buttonHandler() {
    const employeeID: string = this.loginForm.get("employeeID").value;

    if (!employeeID || employeeID.length === 0) {
      this.showErrorToast("No employee ID was provided.");
    } 
    else if (this.buttonActionIndex === 0) this.isEmployeeIDValid({ target: { value: employeeID }, type: "click" });
    else this.logIn();
  }

  /**
   * Submits an authentication request to the server to validate the user's entered credentials,
   * store them in the FingerprintAIO if toggled,
   * and redirect towards the delivery page if authenticated successfully.
   * 
   * @param password A string that is the user's entered or stored password
   */
  public logIn(password?: string) {
    this.logger.info("Attempting to login ...");

    const destinationPage = this.isNewVersion ? ["change-log"] : ["delivery"];

    const postContent: object = {
      requestType: "authentication",
      username: this.loginForm.get("employeeID").value,
      password: password ? password : this.loginForm.get("password").value
    };

    const headers = new HttpHeaders();

    this.httpClient.post(this.global.__serverDomainName + "/auth", postContent, { headers, withCredentials: true })
      .subscribe(
        result => {
          if (result["jwt"]) {
            const jwtToken = result["jwt"];
  
            this.auth.setAuthToken(jwtToken)
              .then(() => {
                // Sets appVersion in storage after token has been set, so that user is guaranteed to see the change log or tour
                if (this.isNewVersion) this.global.setAppVersion();

                // If the "Use biometric login" checkbox has been toggled
                if (this.loginForm.get("useBiometrics").value !== this.global.appSettings["useBiometricLogin"]) {

                  // Register password in FingerprintAIO
                  if (this.loginForm.get("useBiometrics").value) {
                    this.biometricsPlugin.registerBiometricSecret({
                      title: "Registering Usage",
                      subtitle: "Longbottom - Delivery Driver",
                      secret: postContent["password"]
                    })
                      .then(results => {
                        this.logger.info(results);

                        // useBiometrics is being enabled
                        // Update and save useBiometrics setting
                        this.global.appSettings["useBiometricLogin"] = true;
                        this.global.saveSettings();

                        this.router.navigate(destinationPage);
                      })
                      .catch(error => {
                        this.logger.error("logIn(): FingerprintAIO encountered problems storing the password");
                        this.logger.error(error);

                        if (error === "cordova_not_available") {
                          // User is probably a developer running this app in a browser
                          this.router.navigate(destinationPage);
                        }

                        switch (error.code) {
                          case this.biometricsPlugin.BIOMETRIC_DISMISSED:
                          case this.biometricsPlugin.BIOMETRIC_PIN_OR_PATTERN_DISMISSED:
                            this.showErrorToast("The biometric prompt was dismissed. Try again.");

                            break;
                          case this.biometricsPlugin.BIOMETRIC_HARDWARE_NOT_SUPPORTED:
                          case this.biometricsPlugin.BIOMETRIC_SDK_NOT_SUPPORTED:
                            this.loginForm.get("useBiometrics").setValue(false);
                            this.showErrorToast("Your device doesn't support biometric authentication.");

                            break;
                          case this.biometricsPlugin.BIOMETRIC_UNAVAILABLE:
                          case this.biometricsPlugin.BIOMETRIC_INTERNAL_PLUGIN_ERROR:
                            this.loginForm.get("useBiometrics").setValue(false);
                            this.showErrorToast("Biometrics is currently unavailable.");

                            break;
                          default:
                            this.showErrorToast("An unknown error occurred with the biometric login feature.");
                            break;
                        }
                      });
                  } else {
                    // useBiometrics is being disabled
                    // Update and save useBiometrics setting
                    this.global.appSettings["useBiometricLogin"] = false;
                    this.global.saveSettings();
                  }
                } else {
                  // Continue as normal
                  // FIXME: Everything was successful and acting as normal, but router doesn't navigate to delivery
                  // Only seems to happen with expired tokens
                  console.log("logIn(): Reached point of routing to delivery page");

                  this.router.navigate(destinationPage);
                }
              });
          } else {
            this.showErrorToast("Hey! You skipped something.");
          }
        }, 
        (error: HttpErrorResponse) => {
          this.logger.error("logIn(): There was a problem communicating with the server");
          this.logger.error(error);

          if (error.status === 400) this.showErrorToast("No username was provided");
          else this.showErrorToast("There was a problem communicating with the server.");
        },
        () => {
          this.logger.info("Login transaction with server has successfully completed!");
        }
      );
  }
}