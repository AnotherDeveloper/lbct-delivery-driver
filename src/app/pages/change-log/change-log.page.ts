import { AuthenticationService } from 'src/app/services/authentication.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-change-log',
  templateUrl: './change-log.page.html',
  styleUrls: ['./change-log.page.scss'],
})
export class ChangeLogPage implements OnInit {

  public firstName: string;

  constructor(
    private router: Router,
    private auth: AuthenticationService
  ) { }

  public ngOnInit() {
    const nameObject = this.auth.getName();

    this.firstName = nameObject["first"];
  }

  public routeToDelivery() {
    this.router.navigate(["delivery"]);
  }
}
