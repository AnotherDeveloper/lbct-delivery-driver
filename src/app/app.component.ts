import { NGXLogger } from 'ngx-logger';
import { Component, ViewChild, ElementRef } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';

import { IonRouterOutlet, ModalController, Platform } from '@ionic/angular';
import { BackButtonEvent } from '@ionic/core';

import { filter, pairwise } from 'rxjs/operators';

import { SettingsPage } from './modals/settings/settings.page';
import { FeedbackPage } from './modals/feedback/feedback.page';

import { Global } from 'src/global';
import { AuthenticationService } from './services/authentication.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {

  private previousPageRoute: string = "";
  
  @ViewChild('screen', { static: true })
  private screen: IonRouterOutlet;

  constructor(
    public modalController: ModalController,
    private auth: AuthenticationService,
    private platform: Platform,
    private global: Global,
    private router: Router,
    private logger: NGXLogger
  ) {
    this.initializeApp();
  }

  private initializeApp() {
    this.logger.info("Initializing app");

    this.platform.ready().then(() => {
      this.auth.initAuth()
        .then(() => {
          // If there is an auth token and it's not expired continue as logged in
          if (this.auth.getAuthToken() && !this.auth.isTokenExpired())  {
            this.router.navigate(["delivery"]);
          } else {
            this.router.navigate(["login"]);
          }
        });
        
      this.global.initApp();

      const appV = this.global.__appVersion;
      this.logger.info(`Running version ${appV.major}.${appV.minor}.${appV.patch}`);

      // Prevent user from routing back into the login page
      document.addEventListener("ionBackButton", (event: BackButtonEvent) => {
        event.detail.register(0, () => {
          switch (this.previousPageRoute) {
            case "/login":
            case "/change-log":
            case "/delivery":
              event.preventDefault();

              // TODO: Show modal to confirm if user wants to close the app
              break;
          }
        })
      });


      // Listen for URL changes to remember the last visited URL
      this.router.events
        .pipe(filter(event => event instanceof NavigationEnd))
        .subscribe((event: NavigationEnd) => {
          this.previousPageRoute = event.url;
        });
    });
  }
  
  public async openModal(pagePath: string) {
    let requestedModal = undefined;

    // Identify the page the modal needs to open
    switch (pagePath) {
      case "/settings": requestedModal = SettingsPage; break;
      case "/feedback": requestedModal = FeedbackPage; break;
      default:
        this.logger.warn("App component: openModal(): Unknown modal case was processed");

        break;
    }

    // Creates modal
    const modal = await this.modalController.create({
      component: requestedModal
    });

    // Depending on which page was opened process returned data or run a pre-determined action
    if (pagePath === "/settings") {
      modal.onDidDismiss().then();
      // TODO: Settings don't get instantly applied
        // Reloading the page will cause the user to loose manually entered orders
        // So, have change listeners in the components that have settings then
    }

    return await modal.present();
  }

  /**
   * // TODO: Implement function and write description
   */
  private captureScreen() {
    
  }
}
