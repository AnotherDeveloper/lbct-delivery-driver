import { Injectable } from '@angular/core';
import { Storage } from '@capacitor/storage';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  private jwtToken: string;
  private jwtExpiration: number;
  private userID: string;
  private name: string;

  constructor() { }

  // TODO: Any error needs to visually tell the user there was a problem and show or give a way to share the stacktrace
  // If there is an auth problem, NgxLogger is SOL

  public async initAuth() {
    const storedKeys = await Storage.keys();

    if (storedKeys.keys.includes("authToken"))  {
      await Storage.get({ key: "authToken" })
        .then(token => {
          const authObject = JSON.parse(token.value);

          this.jwtToken = authObject.token;

          this.updatePropertyValues();
        })
        .catch(error => {
          console.error(error);
        });
    }
  }

  /**
   * Updates user/token specific variables
   */
  private updatePropertyValues() {
    const jwtPayloadBase64 = this.jwtToken.split(".")[1];
    const jwtPayload = atob(jwtPayloadBase64);
    const jwtPayloadObject = JSON.parse(jwtPayload);

    this.jwtExpiration = jwtPayloadObject["exp"];
    this.userID = jwtPayloadObject["sub"];
    this.name = jwtPayloadObject["name"];
  }

  public getAuthToken(): string { return this.jwtToken; }
  public getUserID(): string { return this.userID; }
  public getName(): string { return this.name; }
  public isTokenExpired(): boolean { return (Date.now() > this.jwtExpiration); }

  /**
   * Stores the users authentication token
   * 
   * @param token JWT authentication token
   */
  public async setAuthToken(token: object) {
    await Storage.set({ key: "authToken", value: JSON.stringify(token) })
      .then(results => {
        this.jwtToken = token["token"];
        this.updatePropertyValues();
      })
      .catch(error => {
        console.error(error);
      });
  }

  /**
   * Deletes user authentication token
   */
  public async logOut() {
    await Storage.remove({ key: "authToken" })
      .catch(error => {
        console.error(error);
      });
  }
}