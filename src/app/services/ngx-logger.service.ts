import { HttpBackend, HttpHeaders, HttpParams } from '@angular/common/http';
import { ErrorHandler, Injectable } from '@angular/core';
import { NGXLogger, NGXLoggerServerService } from 'ngx-logger';
import { Observable } from 'rxjs';
import { AuthenticationService } from './authentication.service';

@Injectable({
  providedIn: 'root'
})
export class NgxServerService extends NGXLoggerServerService {
    public constructor(
      protected readonly httpBackend: HttpBackend,
      private auth: AuthenticationService
    ) {
      super(httpBackend);
    }

    // TODO: If server is unavailable, use sessionStorage feature and push to server when connection is regained
    // FIXME: Non-string types don't always get converted into strings properly

    protected logOnServer<T>(url: string, logContent: T, options: 
      { headers?: HttpHeaders; reportProgress?: boolean; params?: HttpParams; responseType?: 'arraybuffer' | 'blob' | 'json' | 'text'; withCredentials?: boolean; }): Observable<T> {
        const authToken = this.auth.getAuthToken();

        if (authToken) {
          let headers = options.headers;
          headers = headers.set("Authorization", authToken);

          options.headers = headers;
        }

        return super.logOnServer(url, logContent, options);
    }
}

@Injectable({
  providedIn: 'root'
})
export class RootErrorHandlerService extends ErrorHandler {
  constructor(private logger: NGXLogger) {
    super();
  }

  public handleError(error) {
    super.handleError(error);

    this.logger.error("!!! GLOBALLY CAUGHT !!!", error.stack);
  }
}