import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';

import { Global } from 'src/global';
import { AuthenticationService } from './authentication.service';

@Injectable({
  providedIn: 'root'
})
export class AuthInterceptor implements HttpInterceptor {

  constructor(
    private global: Global,
    private auth: AuthenticationService
  ) {}

  intercept(request: HttpRequest<any>, next: HttpHandler): any {
      const authToken = this.auth.getAuthToken();

      if (authToken && request.url.startsWith(this.global.__serverDomainName)) {
        const clonedReq = request.clone({
          headers: request.headers.set("Authorization", authToken)
        });

        return next.handle(clonedReq);
      } else {
        return next.handle(request);
      }
  }
}
