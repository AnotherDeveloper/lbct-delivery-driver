import { TestBed } from '@angular/core/testing';

import { NgxServerService } from './ngx-logger.service';

describe('NgxServerService', () => {
  let service: NgxServerService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(NgxServerService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
