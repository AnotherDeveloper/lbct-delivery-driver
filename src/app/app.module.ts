import { SettingsPageModule } from './modals/settings/settings.module';
import { FeedbackPageModule } from './modals/feedback/feedback.module';
import { environment } from './../environments/environment';
import { Keys } from './../keys';
import { Global } from './../global';

// Services
import { AuthInterceptor } from './services/interceptors.service';
import { NgxServerService, RootErrorHandlerService } from './services/ngx-logger.service';

// Angular
import { ErrorHandler, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

// Angular bindings?
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { FingerprintAIO } from '@awesome-cordova-plugins/fingerprint-aio/ngx';
import { TextToSpeechAdvanced } from '@awesome-cordova-plugins/text-to-speech-advanced/ngx';
import { Insomnia } from '@awesome-cordova-plugins/insomnia/ngx';
import { LoggerModule, TOKEN_LOGGER_SERVER_SERVICE } from 'ngx-logger';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { AuthenticationService } from './services/authentication.service';
import { DatePipe } from '@angular/common';

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    HttpClientModule,
    FeedbackPageModule,
    SettingsPageModule,
    LoggerModule.forRoot(
      environment.logging,
      {
        serverProvider: {
          provide: TOKEN_LOGGER_SERVER_SERVICE, useClass: NgxServerService
        }
      }
    )
  ],
  providers: [
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    { provide: ErrorHandler, useClass: RootErrorHandlerService },
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true },
    AuthenticationService,
    FingerprintAIO,
    TextToSpeechAdvanced,
    Insomnia,
    Storage,
    Global,
    Keys,
    DatePipe
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
