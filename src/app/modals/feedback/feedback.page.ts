import { HttpClient } from '@angular/common/http';
import { FormControl } from '@angular/forms';
import { Component, Input, OnInit, ElementRef } from '@angular/core';

import { ToastController, ModalController } from '@ionic/angular';
import { NGXLogger } from 'ngx-logger';

import { Global } from 'src/global';

@Component({
  selector: 'app-feedback',
  templateUrl: './feedback.page.html',
  styleUrls: ['./feedback.page.scss'],
})
export class FeedbackPage implements OnInit {

  @Input() captureTarget: ElementRef;

  messageType = new FormControl('');
  suggestionType = new FormControl('');
  messageContent = new FormControl('');

  constructor(
    private logger: NGXLogger,
    private global: Global,
    private httpClient: HttpClient,
    private toastController: ToastController,
    private modalController: ModalController
  ) {
    this.logger.info("Feedback page was opened");
  }

  public ngOnInit() { }

  public submitFeedback() {
    const postContent = {
      messageType: this.messageType.value,
      suggestionType: this.suggestionType.value,
      messageContent: this.messageContent.value
    };

    this.httpClient.post(this.global.__serverDomainName + "/feedback", postContent)
      .subscribe(
        async (response) =>  {
          this.logger.info("submitFeedback(): User feedback has been successfully submitted");

          this.messageType.setValue("");
          this.messageContent.setValue("");

          const toast = await this.toastController.create({
            message: "Your feedback was successfully submitted.",
            position: "bottom",
            duration: (30 * 1000), // Seconds * milliseconds
            buttons: [
              {
                text: 'Ok',
                role: 'cancel',
                handler: () => {
                  this.logger.debug("Error message toast was manually closed");
                }
              }
            ]
          });

          await toast.present();
        },
        async (error) => {
          this.logger.error("submitFeedback(): There was a problem submitting user feedback to the server");
          this.logger.error(error);

          let errorMessage: string;

          switch (error.status) {
            case 401: errorMessage = "Your login session has expired."; break;
            case 500: errorMessage = "The server encountered a problem processing your feedback."; break;
            default: errorMessage = "There was an unknown problem while processing your feedback."; break;
          }

          const errorToast = await this.toastController.create({
            message: errorMessage,
            position: 'bottom',
            color: "danger",
            buttons: [
              {
                text: 'Ok',
                role: 'cancel',
                handler: () => {
                  this.logger.debug("Error message toast was manually closed");
                }
              }
            ]
          });

          await errorToast.present();
        },
        () => { }
      );
  }

  public dismissModal() {
    this.modalController.dismiss();
  }
}

// TODO: Report problem, let user select area of the screen
// UI: The input fields placeholder text is slightly inconsistent in color