import { Global } from 'src/global';

import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ModalController } from '@ionic/angular';

import { NGXLogger } from 'ngx-logger';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.page.html',
  styleUrls: ['./settings.page.scss'],
})
export class SettingsPage implements OnInit {
  // TODO: Implement and enable setting features

  private readonly __settingsPageVersion = 1;

  constructor(
    private logger: NGXLogger,
    private global: Global,
    private modalController: ModalController
  ) {
    this.logger.info("Settings page was opened");
  }

  public appSettingsGroup = new FormGroup({
    userInterface: new FormGroup({
      theme: new FormControl(this.global.appSettings["userInterface"]["theme"])
    }),
    mapSettings: new FormGroup({
      caching: new FormControl(this.global.appSettings["mapSettings"]["caching"]),
      rendering: new FormGroup({
        dimensions: new FormControl(this.global.appSettings["mapSettings"]["rendering"]["dimensions"]),
        terrainType: new FormControl(this.global.appSettings["mapSettings"]["rendering"]["terrainType"])
      }),
      showTraffic: new FormControl(this.global.appSettings["mapSettings"]["showTraffic"]),
      optimizeRoute: new FormControl(this.global.appSettings["mapSettings"]["optimizeRoute"])
    })
  });

  public ngOnInit() {
    console.warn(this.global.appSettings);
  }

  public dismissModal() {
    // Checking for user's theme settings
    const prefersDark = window.matchMedia('(prefers-color-scheme: dark)');

    switch (this.global.appSettings["userInterface"]["theme"]) {
      case "system": document.body.classList.toggle("dark", prefersDark.matches); break;
      case "light": document.body.classList.toggle("dark", false); break;
      case "dark": document.body.classList.toggle("dark", true); break;
    }

    this.applyChanges();

    this.modalController.dismiss();
  }

  /**
   * Event handler to change the theme on the go, so the user can see what has changed
   */
  public uiThemeChange(event) {
    document.body.classList.toggle("dark", event.detail.value === "dark" ? true : false);
  }

  /**
   * Applies all changes from the SettingsPage form values to the Global.appSettings object, and saves them
   */
  public applyChanges() {
    if (this.global.appSettings["settingsVersion"] === this.__settingsPageVersion) {
      let appSettingsChanges = this.appSettingsGroup.value;
      appSettingsChanges["settingsVersion"] = this.__settingsPageVersion;

      this.global.appSettings = appSettingsChanges;
      this.global.saveSettings();
    } else {
      this.logger.error("Version miss-match! Not applying changes");
      this.logger.error(`Global.__settingsVersion: ${this.global.appSettings["settingsVersion"]}, SettingsPage.__settingsPageVersion: ${this.__settingsPageVersion}`);
    }
  }

  /**
   * Reverts all SettingsPage form values to the last saved appSettings values
   */
  public revertChanges() {
    this.appSettingsGroup.patchValue(this.global.appSettings);
  }
}