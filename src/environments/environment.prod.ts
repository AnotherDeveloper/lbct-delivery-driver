import { NgxLoggerLevel } from "ngx-logger";
import { INGXLoggerConfig } from "ngx-logger/lib/config/iconfig";

const logging: INGXLoggerConfig = {
  serverLoggingUrl: "https://longbottom-coffee-n-tea.uw.r.appspot.com/logging",
  level: NgxLoggerLevel.TRACE,
  serverLogLevel: NgxLoggerLevel.INFO,
  timestampFormat: "YYYY-MM-dd HH:mm:ss:SSS"
}

export const environment = {
  production: true,
  serverDomain: "https://longbottom-coffee-n-tea.uw.r.appspot.com",
  logging
};
